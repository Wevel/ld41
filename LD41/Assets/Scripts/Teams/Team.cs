﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Team
{
	public readonly Map map;
	public readonly Color unitColour;
	public readonly Color buildingColour;
	public readonly int baseFleetLimit;
	public readonly Texture2D shipTexture;

	public int platinumCount { get; private set; }
	public int aluminiumCount { get; private set; }
	public int waterCount { get; private set; }

	public int fleetLimit { get; private set; }
	public int fleetUsage { get; private set; }

	public Team (Map map, Color unitColour, Color buildingColour, Texture2D shipTexture, int baseFleetLimit)
	{
		this.map = map;
		this.unitColour = unitColour;
		this.buildingColour = buildingColour;
		this.baseFleetLimit = baseFleetLimit;
		this.shipTexture = shipTexture;

		platinumCount = 0;
		aluminiumCount = 0;
		waterCount = 0;

		fleetUsage = 0;
		RecalculateFleetLimit ();
	}

	public void RecalculateFleetLimit ()
	{
		fleetLimit = baseFleetLimit;

		foreach (Building item in map.Buildings)
		{
			if (item.team == this && item.hasBeenBuiltFully)
			{
				fleetLimit += item.type.fleetLimitBonus;
			}
		}
	}

	public bool CanSpawnShip (ShipType type)
	{
		return fleetUsage + type.fleetLimitCost <= fleetLimit;
	}

	public bool CanQueueShip (ShipType type)
	{
		return fleetUsage + type.fleetLimitCost <= fleetLimit
			&& type.platinumCost <= platinumCount
			&& type.aluminiumCost <= aluminiumCount
			&& type.waterCost <= waterCount;
	}

	public bool CanMakeBuilding (BuildingType type)
	{
		return type.platinumCost <= platinumCount
			&& type.aluminiumCost <= aluminiumCount
			&& type.waterCost <= waterCount;
	}

	public void HasSpawnedShip (ShipType type)
	{
		fleetUsage += type.fleetLimitCost;
	}

	public void HasQueuedUpShipBuild (ShipType type)
	{
		platinumCount -= type.platinumCost;
		aluminiumCount -= type.aluminiumCost;
		waterCount -= type.waterCost;
	}

	public void HasCanceledShipBuild (ShipType type)
	{
		platinumCount += type.platinumCost;
		aluminiumCount += type.aluminiumCost;
		waterCount += type.waterCost;
	}

	public void HasBuildBuilding (BuildingType type)
	{
		platinumCount -= type.platinumCost;
		aluminiumCount -= type.aluminiumCost;
		waterCount -= type.waterCost;
	}

	public void RemoveFleetUsage (int count)
	{
		fleetUsage -= count;
	}

	public void AddResources (ResourceType type, int count)
	{
		switch (type)
		{
			case ResourceType.Water:
				waterCount += count;
				break;
			case ResourceType.Aluminium:
				aluminiumCount += count;
				break;
			case ResourceType.Platinum:
				platinumCount += count;
				break;
			case ResourceType.None:
			default:
				Debug.Log ("No resource type given to add to");
				break;
		}
	}
}
