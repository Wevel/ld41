﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiniteStateMachine<T>
{
	public readonly T stateItem;

	public IState<T> currentState { get; private set; }
	public IState<T> lastState { get; private set; }

	public FiniteStateMachine(T stateItem, IState<T> initialState)
	{
		this.stateItem = stateItem;

		lastState = null;

		SetState (initialState);
	}

	public void Update (float deltaTime)
	{
		if (currentState != null) currentState.OnMaintainState (this, deltaTime);
	}

	public void SetState (IState<T> state)
	{
		if (state == null) throw new System.Exception ("Can't go to a null state");

		if (currentState != null) currentState.OnExitState (this);

		lastState = currentState;
		currentState = state;

		currentState.OnEnterState (this);
	}

	public void RevertToLastState ()
	{
		SetState (lastState);
	}

	public bool IsInState<StateType> ()
	{
		return currentState != null && currentState.GetType () == typeof (StateType);
	}

	public bool IsInState (IState<T> state)
	{
		return currentState == state;
	}
}
