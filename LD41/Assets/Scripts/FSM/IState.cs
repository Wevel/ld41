﻿public interface IState<T>
{
	void OnEnterState (FiniteStateMachine<T> stateMachine);
	void OnMaintainState (FiniteStateMachine<T> stateMachine, float deltaTime);
	void OnExitState (FiniteStateMachine<T> stateMachine);
}
