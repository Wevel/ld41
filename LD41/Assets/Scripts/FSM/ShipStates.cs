﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ShipStates
{
	public class Wander : IState<Ship>
	{
		public Vector2 wanderArround;

		public Wander (Vector2 wanderArround)
		{
			this.wanderArround = wanderArround;
		}

		public void OnEnterState (FiniteStateMachine<Ship> stateMachine) { }

		public void OnMaintainState (FiniteStateMachine<Ship> stateMachine, float deltaTime)
		{
			Vector2 steeringForce = Vector2.zero;

			steeringForce += stateMachine.stateItem.Arrive (wanderArround, 0.3f) * 0.25f;
			steeringForce += stateMachine.stateItem.Wander (deltaTime) * 2;
			steeringForce += stateMachine.stateItem.Seperation (4, (agent) => { return true; }) * 2;
			steeringForce += stateMachine.stateItem.Cohesion (10, (agent) => { return true; });

			stateMachine.stateItem.SetSteeringForce (steeringForce);
		}

		public void OnExitState (FiniteStateMachine<Ship> stateMachine) { }
	}

	public class FollowGroup : IState<Ship>
	{
		public Ship leader = null;
		public Vector2 offset;

		public FollowGroup (Ship leader, Vector2 offset)
		{
			this.leader = leader;
			this.offset = offset;
		}

		public void OnEnterState (FiniteStateMachine<Ship> stateMachine) { }

		public void OnMaintainState (FiniteStateMachine<Ship> stateMachine, float deltaTime)
		{
			Debug.DrawLine (stateMachine.stateItem.position, leader.position + (Vector2)(leader.rotation * offset), Color.black);

			Vector2 steeringForce = Vector2.zero;

			steeringForce += stateMachine.stateItem.OffsetPursuit (leader, offset) * 3;
			//steeringForce += stateMachine.stateItem.Seperation (0.5f, (agent) => { return true; }) * 2;
			//steeringForce += stateMachine.stateItem.Cohesion (10, (agent) => { return true; });

			stateMachine.stateItem.SetSteeringForce (steeringForce);

			// If the leader is now idle, then we need to go to idle as well
			if (leader.stateMachine.IsInState<Idle> ())
			{
				Vector2 worldOffset = leader.rotation * offset;
				stateMachine.SetState (new Idle (leader.position + worldOffset));
			}
		}

		public void OnExitState (FiniteStateMachine<Ship> stateMachine) { }
	}

	public class Patrol : IState<Ship>
	{
		public Path path;
		public float speedPercent;

		public Patrol (Path path, float speedPercent)
		{
			this.path = path;
			this.speedPercent = speedPercent;
		}

		public void OnEnterState (FiniteStateMachine<Ship> stateMachine)
		{
			stateMachine.stateItem.SetMaxSpeedPercent (speedPercent);
		}

		public void OnMaintainState (FiniteStateMachine<Ship> stateMachine, float deltaTime)
		{
			Vector2 steeringForce = Vector2.zero;

			steeringForce += stateMachine.stateItem.FollowPath (path, 1f) * 2;
			steeringForce += stateMachine.stateItem.Seperation (2, (agent) => { return true; }) * 2;
			steeringForce += stateMachine.stateItem.Cohesion (10, (agent) => { return true; });

			stateMachine.stateItem.SetSteeringForce (steeringForce);

			if (path.Finished) path.Restart ();
		}

		public void OnExitState (FiniteStateMachine<Ship> stateMachine)
		{
			stateMachine.stateItem.SetMaxSpeedPercent (1);
		}
	}

	public class GotoLocation : IState<Ship>
	{
		public Vector2 location;
		public float speedPercent;

		public GotoLocation (Vector2 location, float speedPercent)
		{
			this.location = location;
			this.speedPercent = speedPercent;
		}

		public void OnEnterState (FiniteStateMachine<Ship> stateMachine)
		{
			stateMachine.stateItem.SetMaxSpeedPercent (speedPercent);
		}

		public void OnMaintainState (FiniteStateMachine<Ship> stateMachine, float deltaTime)
		{
			Vector2 steeringForce = Vector2.zero;

			steeringForce += stateMachine.stateItem.Wander (deltaTime);
			steeringForce += stateMachine.stateItem.Arrive (location, 0.4f);
			steeringForce += stateMachine.stateItem.Seperation (2, (agent) => { return true; }) * 2;
			steeringForce += stateMachine.stateItem.Cohesion (10, (agent) => { return true; });

			stateMachine.stateItem.SetSteeringForce (steeringForce);

			if ((location - stateMachine.stateItem.position).sqrMagnitude < 1) stateMachine.SetState (new Idle (location));
		}

		public void OnExitState (FiniteStateMachine<Ship> stateMachine)
		{
			stateMachine.stateItem.SetMaxSpeedPercent (1);
		}
	}

	public class FollowComandQueue : IState<Ship>
	{
		public CommandQueue comandQueue;
		public float speedPercent;

		public FollowComandQueue (CommandQueue comandQueue, float speedPercent)
		{
			this.comandQueue = comandQueue;
			this.speedPercent = speedPercent;
		}

		public void OnEnterState (FiniteStateMachine<Ship> stateMachine)
		{
			stateMachine.stateItem.SetMaxSpeedPercent (speedPercent);
		}

		public void OnMaintainState (FiniteStateMachine<Ship> stateMachine, float deltaTime)
		{
			Vector2 steeringForce = Vector2.zero;

			steeringForce += stateMachine.stateItem.FollowComandQueue (comandQueue, deltaTime) * 2;
			steeringForce += stateMachine.stateItem.Seperation (2, (agent) => { return true; }) * 2;
			steeringForce += stateMachine.stateItem.Cohesion (10, (agent) => { return true; });

			stateMachine.stateItem.SetSteeringForce (steeringForce);

			// If the queue is finished, idle about the current position
			// I don't want the ships to wander as they wont stay in formation like that
			if (comandQueue.Finished) stateMachine.SetState (new Idle (stateMachine.stateItem.position));
		}

		public void OnExitState (FiniteStateMachine<Ship> stateMachine)
		{
			stateMachine.stateItem.SetMaxSpeedPercent (1);
		}
	}

	public class Idle : IState<Ship>
	{
		public Vector2 position;

		public Idle (Vector2 position)
		{
			this.position = position;
		}

		public void OnEnterState (FiniteStateMachine<Ship> stateMachine) { }

		public void OnMaintainState (FiniteStateMachine<Ship> stateMachine, float deltaTime)
		{
			Vector2 steeringForce = Vector2.zero;

			if ((position - stateMachine.stateItem.position).sqrMagnitude > 0.5f)
				steeringForce += stateMachine.stateItem.Arrive (position, 0.5f) * 2f;
			else
				steeringForce -= stateMachine.stateItem.velocity * stateMachine.stateItem.mass * 1.5f;

			steeringForce += stateMachine.stateItem.Seperation (1.5f, (agent) => { return true; }) * 0.75f;
			//steeringForce += stateMachine.stateItem.Alignment (1.5f, (agent) => { return true; }) * 0.25f;
			//steeringForce += stateMachine.stateItem.Cohesion (3, (agent) => { return true; }) * 0.25f;

			stateMachine.stateItem.SetSteeringForce (steeringForce);
		}

		public void OnExitState (FiniteStateMachine<Ship> stateMachine) { }
	}

	public class Enemy : IState<Ship>
	{
		public Path mainPath;

		public Enemy (Path mainPath)
		{
			this.mainPath = new Path(mainPath);
		}

		public void OnEnterState (FiniteStateMachine<Ship> stateMachine) { }

		public void OnMaintainState (FiniteStateMachine<Ship> stateMachine, float deltaTime)
		{
			Vector2 steeringForce = Vector2.zero;

			steeringForce += stateMachine.stateItem.FollowPath (mainPath, 2f) * 2;
			steeringForce += stateMachine.stateItem.Seperation (2, (agent) => { return true; }) * 2;
			steeringForce += stateMachine.stateItem.Cohesion (10, (agent) => { return true; });

			stateMachine.stateItem.SetSteeringForce (steeringForce);

			if (mainPath.Finished) stateMachine.SetState (new AttackPlayerCommand ());
		}

		public void OnExitState (FiniteStateMachine<Ship> stateMachine) { }
	}

	public class AttackPlayerCommand : IState<Ship>
	{
		public AttackPlayerCommand () { }

		public void OnEnterState (FiniteStateMachine<Ship> stateMachine) { }

		public void OnMaintainState (FiniteStateMachine<Ship> stateMachine, float deltaTime)
		{
			const float targetDistance = 4f;

			Vector2 steeringForce = Vector2.zero;

			Vector2 fromCommand = stateMachine.stateItem.position - stateMachine.stateItem.map.playerCommandCenter.position;
			fromCommand /= fromCommand.magnitude;
			fromCommand *= targetDistance;

			Vector2 targetPosition = stateMachine.stateItem.map.playerCommandCenter.position + fromCommand;

			steeringForce += stateMachine.stateItem.Arrive (targetPosition, 0.4f);
			steeringForce += stateMachine.stateItem.Seperation (2, (agent) => { return true; }) * 2;
			steeringForce += stateMachine.stateItem.Cohesion (10, (agent) => { return true; });

			stateMachine.stateItem.SetSteeringForce (steeringForce);
		}

		public void OnExitState (FiniteStateMachine<Ship> stateMachine) { }
	}
}
