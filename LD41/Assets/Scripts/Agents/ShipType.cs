﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipType
{
	public delegate Ship SpawnShip (Map map, Team team, ShipType type, Vector2 position, float direction);

	public readonly string name;
	public readonly float spawnTime;
	public readonly float collisionRange;
	public readonly int maxHealth;
	public readonly int fleetLimitCost;
	public readonly Sprite icon;
	public SpawnShip spawnShip;

	public readonly int platinumCost;
	public readonly int aluminiumCost;
	public readonly int waterCost;

	public Vector2 collisionOffset = Vector2.zero;

	public ShipType (string name, float spawnTime, float collisionRange, int fleetLimitCost, int platinumCost, int aluminiumCost, int waterCost, int maxHealth, Sprite icon, SpawnShip spawnShip)
	{
		this.name = name;
		this.spawnTime = spawnTime;
		this.collisionRange = collisionRange;
		this.fleetLimitCost = fleetLimitCost;
		this.icon = icon;
		this.spawnShip = spawnShip;
		this.maxHealth = maxHealth;
		this.platinumCost = platinumCost;
		this.aluminiumCost = aluminiumCost;
		this.waterCost = waterCost;
	}

	public Ship SpawnShipNow (Map map, Team team, Vector2 position, float direction)
	{
		Ship ship = spawnShip (map, team, this, position, direction);
		ship.collisionRadius = collisionRange;
		ship.colliderOffset = collisionOffset;

		return ship;
	}
}
