﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ship : Agent, ISelectable, ITargetable
{
	public readonly FiniteStateMachine<Ship> stateMachine;
	public readonly ShipType type;

	private Vector2 steeringForce = Vector2.zero;
	private Vector2 additionalSteeringForce;

	public Ship (Map map, Team team, ShipType type, Vector2 position, float direction) : base (map, team, position, direction)
	{
		this.type = type;
		this.currentHealth = type.maxHealth;
		stateMachine = new FiniteStateMachine<Ship> (this, new ShipStates.Wander (position));

		team.HasSpawnedShip (type);
	}

	public void SetSteeringForce (Vector2 steeringForce)
	{
		this.steeringForce = steeringForce + additionalSteeringForce;
		additionalSteeringForce = Vector2.zero;
	}

	public Vector2 targetPosition
	{
		get
		{
			return position;
		}
	}

	public abstract float GetStrengthFactor ();
	
	public string name
	{
		get
		{
			return type.name;
		}
	}

	public Sprite icon
	{
		get
		{
			return type.icon;
		}
	}

	public int currentHealth { get; private set; }

	public int maxHealth
	{
		get
		{
			return type.maxHealth;
		}
	}

	public bool isAlive
	{
		get
		{
			return currentHealth > 0;
		}
	}

	public virtual void TryQueueBuild (string name, Vector2 at) { }

	protected override void OnUpdate (float deltaTime)
	{
		stateMachine.Update (deltaTime);
	}

	protected override Vector2 CalculateSteeringForce (float deltaTime)
	{
		return steeringForce;
	}

	public override void OnCollisionStay (ICollidable other)
	{
		if (other is Ship)
		{
			Ship otherShip = (Ship)other;

			if (stateMachine.IsInState<ShipStates.FollowGroup> ()
				&& otherShip.stateMachine.IsInState<ShipStates.FollowGroup> ())
			{
				ShipStates.FollowGroup thisState = (ShipStates.FollowGroup)this.stateMachine.currentState;
				ShipStates.FollowGroup otherState = (ShipStates.FollowGroup)otherShip.stateMachine.currentState;

				// Make sure we are in the same group to make sure things don't get confused by different groups colliding
				if (thisState.leader == otherState.leader)
				{
					// If both ships are colliding with each other and the other is infront of this one
					// Then it is likely that this one is stuck behind the other
					// If so then we can swap the offsets in the group

					float deltaDot = Vector2.Dot (heading, (otherShip.position - this.position).normalized);
					if (deltaDot > 0.95f) // cos (18) = 0.95
					{

						float offsetDot = Vector2.Dot (heading, ((Vector2)(thisState.leader.rotation * thisState.offset) + thisState.leader.position - this.position).normalized);
						if (offsetDot > 0.95f)
						{
							Vector2 tmpOffset = otherState.offset;
							otherState.offset = thisState.offset;
							thisState.offset = tmpOffset;
						}
					}
				}
			}
			else if (stateMachine.IsInState<ShipStates.Idle> ())
			{
				ShipStates.Idle idleState = (ShipStates.Idle)stateMachine.currentState;
				idleState.position = position;
			}
		}
		else if (other is Building)
		{
			Building otherBuilding = (Building)other;

			// Stop buildings getting stuck on buildings
			Vector2 delta = otherBuilding.position - position;
			float dot = Vector2.Dot (delta, heading);
			Vector2 push = (dot * heading) - delta;
			push /= push.sqrMagnitude * mass;
			additionalSteeringForce = push;
			additionalSteeringForce -= dot * heading * mass;
			additionalSteeringForce *= 2f;

			Debug.DrawRay (otherBuilding.position, additionalSteeringForce, Color.cyan);
		}
		else if (other is Projectile)
		{
			Projectile projectile = (Projectile)other;

			if (projectile.CanDamage && projectile.team != team)
			{
				List<Ship> inRange = map.agentManager.GetAgentsInRange<Ship> (projectile.position, projectile.splashDamageRange, (ship) => 
				{
					return projectile.team != team;
				});

				// Make sure that this ship is encluded in the damaged ships
				if (!inRange.Contains (this)) inRange.Add (this);

				for (int i = 0; i < inRange.Count; i++) inRange[i].currentHealth -= projectile.GetDamage (inRange[i].position, inRange[i] == this);

				projectile.Destroy ();
				if (!isAlive) Destroy ();
			}
		}
	}

	public void DoDamage (int amount)
	{
		currentHealth -= amount;
		if (!isAlive) Destroy ();
	}

	public Vector2 GetInRangePosition (Vector2 otherPosition, float range)
	{
		float distance = Vector2.Distance (position, otherPosition);
		if (distance < range) return otherPosition;

		float moveDistance = distance - range - 1;
		return otherPosition + Vector2.ClampMagnitude (position - otherPosition, moveDistance);
	}

	protected override void OnDestroy ()
	{
		team.RemoveFleetUsage (type.fleetLimitCost);
	}
}
