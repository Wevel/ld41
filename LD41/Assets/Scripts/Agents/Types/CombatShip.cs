﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatShip : Ship
{
	public float attackRange;
	public List<Gun> guns;

	public CombatShip (Map map, Team team, ShipType type, Vector2 position, float direction, float attackRange, params Gun[] guns) : base (map, team, type, position, direction)
	{
		this.attackRange = attackRange;
		this.guns = new List<Gun> (guns);
	}

	protected override void OnUpdate (float deltaTime)
	{
		base.OnUpdate (deltaTime);

		for (int i = 0; i < guns.Count; i++) guns[i].Update (position, heading, deltaTime);
	}

	protected override void OnDestroy ()
	{
		base.OnDestroy ();

		for (int i = 0; i < guns.Count; i++) guns[i].OnDestroy ();
	}

	public override void OnDrawGizmos ()
	{
		base.OnDrawGizmos ();

		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (position, attackRange);
	}

	public override float GetStrengthFactor ()
	{
		float total = 0;

		total += maxHealth / 100f;

		for (int i = 0; i < guns.Count; i++)
		{
			if (guns[i] is LaserGun) total += 10;
			else if (guns[i] is MissileGun) total += 7.5f;
			else if (guns[i] is KineticGun) total += 5f;
		}

		return total;
	}
}
