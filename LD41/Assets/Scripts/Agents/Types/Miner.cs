﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Miner : Ship
{
	public float mineDelay;
	public int capacity;

	private Dictionary<ResourceType, int> resourcesStored = new Dictionary<ResourceType, int> ();

	private Asteroid asteroidInContact = null;
	private float stillIncontactTimer;
	private float mineTimer = 0;

	public IEnumerable<KeyValuePair<ResourceType, int>> CarriedResources
	{
		get
		{
			foreach (KeyValuePair<ResourceType, int> item in resourcesStored) yield return item;
		}
	}

	public Miner (Map map, Team team, ShipType type, Vector2 position, float direction, float mineDelay, int capacity) : base (map, team, type, position, direction)
	{
		this.mineDelay = mineDelay;
		this.capacity = capacity;
	}

	public void CollectResources (ResourceType type, int amount)
	{
		if (resourcesStored.ContainsKey (type)) resourcesStored[type] += amount;
		else resourcesStored[type] = amount;
	}

	public int CapacityUsed ()
	{
		int total = 0;
		foreach (KeyValuePair<ResourceType, int> item in resourcesStored) total += item.Value;
		return total;
	}

	protected override void OnUpdate (float deltaTime)
	{
		base.OnUpdate (deltaTime);

		if (mineTimer < mineDelay) mineTimer += deltaTime;

		if (asteroidInContact != null)
		{
			while (mineTimer > mineDelay && CapacityUsed () < capacity)
			{
				CollectResources (asteroidInContact.resourceType, 1);
				asteroidInContact.Mine (1);

				mineTimer -= mineDelay;
			}
			
			stillIncontactTimer -= deltaTime;
		}

		if (stillIncontactTimer < 0 || (asteroidInContact != null && !asteroidInContact.CanMine ())) asteroidInContact = null;
	}

	public override void OnCollisionStay (ICollidable other)
	{
		base.OnCollisionStay (other);

		if (other is CommandCenter || other is ResourceDepo)
		{
			foreach (KeyValuePair<ResourceType, int> item in resourcesStored) team.AddResources (item.Key, item.Value);
			resourcesStored.Clear ();
		}
		else if (other is Asteroid)
		{
			Asteroid asteroid = (Asteroid)other;

			if (asteroid.CanMine())
			{
				if (stateMachine.IsInState<ShipStates.FollowComandQueue> ())
				{
					ShipStates.FollowComandQueue state = (ShipStates.FollowComandQueue)stateMachine.currentState;
					if (state.comandQueue.CurrentCommand () is MineCommand)
					{
						asteroidInContact = asteroid;
						stillIncontactTimer = 0.2f;
					}
				}
			}
		}
	}

	public override float GetStrengthFactor ()
	{
		return 0;
	}
}
