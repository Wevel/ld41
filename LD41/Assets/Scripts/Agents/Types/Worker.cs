﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worker : Ship
{
	public int buildRate { get; private set; }

	private Building buildableInContact = null;
	private float stillIncontactTimer;

	public Worker (Map map, Team team, ShipType type, Vector2 position, float direction, int buildRate) : base (map, team, type, position, direction)
	{
		this.buildRate = buildRate;
	}

	protected override void OnUpdate (float deltaTime)
	{
		base.OnUpdate (deltaTime);

		if (buildableInContact != null)
		{
			buildableInContact.DoBuildProgress (deltaTime * buildRate);
			stillIncontactTimer -= deltaTime;
		}

		if (stillIncontactTimer < 0) buildableInContact = null;
	}

	public override void OnCollisionStay (ICollidable other)
	{
		base.OnCollisionStay (other);

		if (other is Building)
		{
			Building building = (Building)other;

			if (building.team == team && building.currentHealth < building.maxHealth)
			{
				if (stateMachine.IsInState<ShipStates.FollowComandQueue> ())
				{
					ShipStates.FollowComandQueue state = (ShipStates.FollowComandQueue)stateMachine.currentState;
					if (state.comandQueue.CurrentCommand () is BuildCommand)
					{
						buildableInContact = building;
						stillIncontactTimer = 0.2f;
					}
				}
			}
		}
	}

	public override float GetStrengthFactor ()
	{
		return 0.5f;
	}
}
