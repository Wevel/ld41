﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Agent : ICollidable
{
	#region Steering Behaviours

	public Vector2 Seek (Vector2 target)
	{
		Vector2 targetVelocity = (target - position).normalized * CurrentMaxSpeed;
		return targetVelocity - velocity;
	}

	public Vector2 Flee (Vector2 target)
	{
		Vector2 targetVelocity = (position - target).normalized * CurrentMaxSpeed;
		return targetVelocity - velocity;
	}

	public Vector2 Arrive (Vector2 target, float deceleration)
	{
		Vector2 toTarget = target - position;
		float distance = toTarget.magnitude;

		if (distance > 0)
		{
			float speed = distance / deceleration;
			speed = Mathf.Min (speed, CurrentMaxSpeed);

			Vector2 targetVelocity = toTarget * speed / distance;
			return targetVelocity - velocity;
		}
		else
		{
			return Vector2.zero;
		}
	}

	public Vector2 Pursuit (Agent target)
	{
		Vector2 toTarget = target.position - position;
		float relativeHeading = Vector2.Dot (heading, target.heading);

		if (Vector2.Dot (toTarget, heading) > 0 && relativeHeading < -0.95f) // cos(18) = 0.95
			return Seek (target.position);

		float lookAheadTime = toTarget.magnitude / (CurrentMaxSpeed + target.speed);
		return Seek (target.position + (target.velocity * lookAheadTime));
	}

	public Vector2 Evade (Agent target)
	{
		Vector2 toTarget = target.position - position;

		float lookAheadTime = toTarget.magnitude / (CurrentMaxSpeed + target.speed);
		return Seek (target.position + (target.velocity * lookAheadTime));
	}

	public Vector2 Wander (float deltaTime)
	{
		wanderTarget += new Vector2 (
			Random.Range (-wanderJitter, wanderJitter) * deltaTime,
			Random.Range (-wanderJitter, wanderJitter) * deltaTime);

		wanderTarget.Normalize ();
		wanderTarget *= wanderRadius;

		Vector2 worldTarget = rotation * wanderTarget;
		return worldTarget + (heading * wanderDistance);
	}

	public Vector2 FollowPath (Path path, float wayPointHasArrivedDistance)
	{
		Vector2 next = path.CurrentWaypoint ();
		path.CheckArriveAtWaypoint (position, wayPointHasArrivedDistance);

		if (!path.Finished) return Seek (next);
		else return Arrive (next, 0.6f);
	}

	public Vector2 FollowComandQueue (CommandQueue commandQueue, float deltaTime)
	{
		ICommand next = commandQueue.CurrentCommand ();

		if (next != null)
		{
			commandQueue.CheckCompleteCurrentCommand (this);
			return next.GetSteeringForce (this, deltaTime, commandQueue.Finished);
		}
		else
		{
			Debug.Log ("No commands in command queue");
			return Vector2.zero;
		}
	}

	public Vector2 OffsetPursuit (Agent leader, Vector2 offset)
	{
		Vector2 worldOffset = leader.rotation * offset;
		float lookAheadTime = worldOffset.magnitude / (CurrentMaxSpeed + leader.speed);
		return Arrive (leader.position + worldOffset + (leader.velocity * lookAheadTime), 0.23f);
	}

	public Vector2 Seperation (float range, System.Predicate<Agent> shouldCheck)
	{
		if (shouldCheck == null) shouldCheck = (a) => { return true; };

		Vector2 steering = Vector2.zero;

		Vector2 toNeighbour;

		List<Agent> inRange = map.agentManager.GetAgentsInRange (
			position,
			range,
			(agent) =>
			{
				return agent != this && shouldCheck (agent);
			});

		for (int i = 0; i < inRange.Count; i++)
		{
			toNeighbour = position - inRange[i].position;
			steering += toNeighbour / toNeighbour.sqrMagnitude;
		}

		return steering;
	}

	public Vector2 Alignment (float range, System.Predicate<Agent> shouldCheck)
	{
		if (shouldCheck == null) shouldCheck = (a) => { return true; };

		Vector2 averageHeading = Vector2.zero;

		List<Agent> inRange = map.agentManager.GetAgentsInRange (
			position,
			range,
			(agent) =>
			{
				return agent != this && shouldCheck (agent);
			});

		if (inRange.Count > 0)
		{
			for (int i = 0; i < inRange.Count; i++) averageHeading += inRange[i].heading;

			averageHeading /= (float)inRange.Count;
			return averageHeading - heading;
		}

		return Vector2.zero;
	}

	public Vector2 Cohesion (float range, System.Predicate<Agent> shouldCheck)
	{
		if (shouldCheck == null) shouldCheck = (a) => { return true; };

		Vector2 centreOfMass = Vector2.zero;

		List<Agent> inRange = map.agentManager.GetAgentsInRange (
			position,
			range,
			(agent) =>
			{
				return agent != this && shouldCheck (agent);
			});

		if (inRange.Count > 0)
		{
			for (int i = 0; i < inRange.Count; i++) centreOfMass += inRange[i].position;

			centreOfMass /= (float)inRange.Count;
			return Seek (centreOfMass);
		}

		return Vector2.zero;
	}

	#endregion

	public readonly Map map;
	public readonly Team team;

	public float mass = 1;
	public float maxSpeed = 4;
	public float maxAcceleration = 4;
	public float maxAngleDelta = 200;
	public float collisionRadius = 0.25f;
	public Vector2 colliderOffset = Vector2.zero;

	public float maxSpeedPercent { get; private set; }

	public Vector2 position { get; private set; }
	public Quaternion rotation { get; private set; }
	public Vector2 velocity { get; private set; }
	public Vector2 heading { get; private set; }
	public float speed { get; private set; }

	public Vector2 boundsSize
	{
		get
		{
			return Vector2.one;
		}
	}

	public float wanderDistance = 1;
	public float wanderRadius = 1;
	public float wanderJitter = 2;
	private Vector2 wanderTarget = Vector2.zero;

	public AgentDisplay display { get; private set; }

	public float CollisionTestRange
	{
		get
		{
			return collisionRadius * 2f;
		}
	}

	public float CurrentMaxSpeed
	{
		get
		{
			return maxSpeed * maxSpeedPercent;
		}
	}

	protected Agent (Map map, Team team, Vector2 position, float direction)
	{
		this.map = map;
		this.position = position;
		this.team = team;

		velocity = Vector2.zero;
		heading = Vector2.zero;
		speed = 0;
		maxSpeedPercent = 1;
		rotation = Quaternion.Euler (0, 0, direction);

		display = null;

		map.AddCollidable (this);
		map.agentManager.AddAgent (this);
	}

	public void Update (float deltaTime)
	{
		OnUpdate (deltaTime);

		Vector2 steeringForce = CalculateSteeringForce (deltaTime);
		Vector2 acceleration = Vector2.ClampMagnitude (steeringForce / mass, maxAcceleration);

		velocity += acceleration * deltaTime;
		velocity = Vector2.ClampMagnitude (velocity, CurrentMaxSpeed);
		speed = velocity.magnitude;


		if (velocity.sqrMagnitude > 0.02)
		{
			position = map.GetMoveToPosition (this, deltaTime);
			//position += velocity * deltaTime;

			float targetRotation = Mathf.Atan2 (velocity.y, velocity.x) * Mathf.Rad2Deg;

			rotation = Quaternion.Euler (0, 0, Mathf.MoveTowardsAngle (rotation.eulerAngles.z, targetRotation, maxAngleDelta * deltaTime));
			heading = velocity.normalized;
		}
		else
		{
			// This will update collision, but not make the ship move
			position = map.GetMoveToPosition (this, 0);
		}
	}

	public void Destroy ()
	{
		map.RemoveCollidable (this);
		map.agentManager.RemoveAgent (this);
		display.SetAgent (null);
		OnDestroy ();
	}

	public void SetDisplay (AgentDisplay display)
	{
		if (this.display != null) this.display.SetAgent (null);
		if (display != null) display.SetAgent (this);
		this.display = display;
	}

	public void SetMaxSpeedPercent (float percent)
	{
		maxSpeedPercent = Mathf.Clamp01 (percent);
	}

	public bool DoesCollide (Agent agent, float deltaTime, ref Vector2 positionDelta)
	{
		if (agent != this)
		{
			Vector2 agentPositionDelta = this.position - agent.position - (Vector2)(rotation * colliderOffset);
			if (Vector2.Dot (positionDelta, agentPositionDelta) > 0)
			{
				float seperation = agentPositionDelta.magnitude;
				float dist = seperation - agent.collisionRadius - this.collisionRadius;

				if (dist > 0)
				{
					float projection = Vector2.Dot (positionDelta, agentPositionDelta) / seperation;
					if (dist < projection)
					{
						positionDelta *= Mathf.Clamp01 (dist / projection);
						return true;
					}
				}
				else
				{
					// We already overlap
					positionDelta += agentPositionDelta * dist;
					return true;
				}
			}
		}

		return false;
	}

	public virtual void OnCollisionStay (ICollidable other) { }
	protected virtual void OnUpdate (float deltaTime) { }
	protected virtual void OnDestroy () { }

	protected abstract Vector2 CalculateSteeringForce (float deltaTime);

	public virtual void OnDrawGizmos ()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere (position, collisionRadius);
	}
}
