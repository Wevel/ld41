﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildCommand : ICommand
{
	public Building target;

	public BuildCommand (Building target)
	{
		this.target = target;
	}

	public bool IsComplete (Agent agent)
	{
		return target.Built;
	}

	public Vector2 GetSteeringForce (Agent agent, float deltaTime, bool isFinished)
	{
		return agent.Arrive (target.position, 0.4f);
	}
}
