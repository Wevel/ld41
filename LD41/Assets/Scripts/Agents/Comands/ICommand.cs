﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICommand
{
	bool IsComplete (Agent agent);
	Vector2 GetSteeringForce (Agent agent, float deltaTime, bool isFinished);
}
