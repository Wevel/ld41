﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandQueue
{
	private List<ICommand> commands = new List<ICommand> ();
	private int targetIndex = 0;

	public int NodeCount
	{
		get
		{
			return commands.Count;
		}
	}

	public bool Finished
	{
		get
		{
			return targetIndex > commands.Count - 1;
		}
	}

	public IEnumerable<ICommand> Nodes
	{
		get
		{
			if (!Finished)
			{
				for (int i = targetIndex; i < commands.Count; i++) yield return commands[i];
			}
		}
	}

	public CommandQueue (List<ICommand> commands)
	{
		this.commands = new List<ICommand> (commands);
	}

	public CommandQueue (params ICommand[] commands)
	{
		this.commands = new List<ICommand> (commands);
	}

	public void AddCommand (ICommand command)
	{
		commands.Add (command);
	}

	public ICommand CurrentCommand ()
	{
		if (commands.Count > 0) return commands[Mathf.Clamp (targetIndex, 0, commands.Count - 1)];
		else return null;
	}

	public void CheckCompleteCurrentCommand (Agent agent)
	{
		if (commands[Mathf.Clamp (targetIndex, 0, commands.Count - 1)].IsComplete(agent)) targetIndex++;
	}

	public void Restart ()
	{
		targetIndex = 0;
	}
}
