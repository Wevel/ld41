﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackCommand : ICommand
{
	public ITargetable target;
	public float targetRange;

	public AttackCommand (ITargetable target, float targetRange)
	{
		this.target = target;
		this.targetRange = targetRange;
	}

	public bool IsComplete (Agent agent)
	{
		return !target.isAlive;
	}

	public Vector2 GetSteeringForce (Agent agent, float deltaTime, bool isFinished)
	{
		if (!isFinished) return agent.Seek (target.GetInRangePosition (agent.position, targetRange));
		else return agent.Arrive (target.GetInRangePosition (agent.position, targetRange), 0.4f);
	}
}
