﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCommand : ICommand
{
	public Vector2 target;
	public float arriveDistance;

	public MoveCommand (Vector2 target, float arriveDistance)
	{
		this.target = target;
		this.arriveDistance = arriveDistance;
	}

	public bool IsComplete (Agent agent)
	{
		return (target - agent.position).sqrMagnitude < arriveDistance * arriveDistance;
	}

	public Vector2 GetSteeringForce (Agent agent, float deltaTime, bool isFinished)
	{
		if (!isFinished) return agent.Seek (target);
		else return agent.Arrive (target, 0.4f);
	}
}
