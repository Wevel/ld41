﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineCommand : ICommand
{
	public Asteroid target;

	public MineCommand (Asteroid target)
	{
		this.target = target;
	}

	public bool IsComplete (Agent agent)
	{
		if (agent is Miner) return target.currentHealth <= 0;
		else return true;
	}

	public Vector2 GetSteeringForce (Agent agent, float deltaTime, bool isFinished)
	{
		if (agent is Miner)
		{
			Miner miner = (Miner)agent;

			if (miner.CapacityUsed () < miner.capacity) return agent.Arrive (target.position, 0.4f);
			else return agent.Arrive (getClosestDepoLocation (agent), 0.4f);
		}

		return agent.Arrive (target.position, 0.4f);
	}

	private Vector2 getClosestDepoLocation (Agent agent)
	{
		Vector2 closest = agent.position;
		float bestDistance = float.MaxValue;
		float tmpDistance;

		foreach (Building item in agent.map.Buildings)
		{
			if (item.team == agent.team && item.hasBeenBuiltFully)
			{
				if (item is CommandCenter || item is ResourceDepo)
				{
					tmpDistance = (agent.position - item.position).sqrMagnitude;
					if (tmpDistance < bestDistance)
					{
						bestDistance = tmpDistance;
						closest = item.position;
					}
				}
			}
		}

		return closest;
	}
}
