﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentDisplay : MonoBehaviour
{
	private static Transform holder;
	private static Queue<AgentDisplay> unusedDisplayes = new Queue<AgentDisplay> ();

	static AgentDisplay ()
	{
		holder = new GameObject ("AgentDisplayHolder").transform;
	}

	public static void AddDisplay (Agent agent, Mesh mesh, Material material)
	{
		AgentDisplay display;

		if (unusedDisplayes.Count > 0)
		{
			display = unusedDisplayes.Dequeue ();
			display.gameObject.SetActive (true);
		}
		else
		{
			GameObject go = new GameObject ("AgentDisplay");
			go.transform.SetParent (holder);
			go.AddComponent<MeshFilter> ();
			go.AddComponent<MeshRenderer> ();
			display = go.AddComponent<AgentDisplay> ();
		}

		display.mesh = mesh;

		if (display.material != null) Destroy (display.material);
		display.material = new Material (material);
		display.material.mainTexture = agent.team.shipTexture;

		agent.SetDisplay (display);
	}

	private static void stopUsingDisplay (AgentDisplay display)
	{
		display.gameObject.SetActive (false);
		unusedDisplayes.Enqueue (display);
	}
	
	public Mesh mesh;
	private Material material;

	private Agent agent = null;

	private MeshFilter mf;
	private MeshRenderer mr;

	private void Awake ()
	{
		mf = this.GetComponent<MeshFilter> ();
		mr = this.GetComponent<MeshRenderer> ();
	}

	private void Update ()
	{
		if (agent != null)
		{
			updatePosition ();
			updateDisplay ();
		}
	}

	private void OnDestroy ()
	{
		if (material != null) Destroy (material);
	}

	public void SetAgent (Agent agent)
	{
		this.agent = agent;

		if (agent != null)
		{
			updatePosition ();
			updateDisplay ();
		}
		else
		{
			stopUsingDisplay (this);
		}
	}

	private void updatePosition ()
	{
		transform.position = agent.position;
		transform.rotation = agent.rotation;
	}

	private void updateDisplay ()
	{
		Material[] materials = new Material[mesh.subMeshCount];
		for (int i = 0; i < mesh.subMeshCount; i++) materials[i] = material;
		mr.sharedMaterials = materials;

		if (agent == null) mf.mesh = null;
		else mf.mesh = mesh;
	}
}
