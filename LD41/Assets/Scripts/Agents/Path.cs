﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path
{
	private List<Vector2> path = new List<Vector2> ();
	private int targetIndex = 0;

	public Vector2 Start
	{
		get
		{
			if (path.Count > 0) return path[0];
			else return Vector2.zero;
		}
	}

	public int NodeCount
	{
		get
		{
			return path.Count;
		}
	}

	public bool Finished
	{
		get
		{
			return targetIndex > path.Count - 1;
		}
	}

	public IEnumerable<Vector2> Nodes
	{
		get
		{
			if (!Finished)
			{
				for (int i = targetIndex; i < path.Count; i++) yield return path[i];
			}
		}
	}

	public Path (Path copy)
	{
		this.path = new List<Vector2> (copy.path);
	}

	public Path (List<Vector2> path)
	{
		this.path = new List<Vector2> (path);
	}

	public Path (params Vector2[] path)
	{
		this.path = new List<Vector2> (path);
	}

	public void AddWaypoint (Vector2 newWaypoint)
	{
		path.Add (newWaypoint);
	}

	public Vector2 CurrentWaypoint ()
	{
		if (path.Count > 0) return path[Mathf.Clamp (targetIndex, 0, path.Count - 1)];
		else return Vector2.zero;
	}

	public void CheckArriveAtWaypoint (Vector2 position, float wayPointHasArrivedDistance)
	{
		if ((path[Mathf.Clamp(targetIndex, 0, path.Count - 1)] - position).sqrMagnitude < wayPointHasArrivedDistance * wayPointHasArrivedDistance)
		{
			targetIndex++;
		}
	}

	public void Restart ()
	{
		targetIndex = 0;
	}
}
