﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Gun
{
	public readonly Map map;
	public readonly Team team;
	public readonly Vector2 offset;

	public float targetLocationDetectionRange = 1f;
	public float range = 10;
	public float fireDelay = 1;
	public float resetTargetDelay = 0.5f;
	public TurretTargetMode targetMode = TurretTargetMode.ClosestToEnd;

	public ITargetable currentTarget { get; private set; }

	private bool playerAssignedTarget = false;
	private float resetTargetTimer;
	private float fireTimer = 0;

	public Vector2 position { get; private set; }

	protected Gun (Map map, Team team, Vector2 offset, float range, float fireDelay)
	{
		position = offset;
		this.map = map;
		this.team = team;
		this.offset = offset;
		this.range = range;
		this.fireDelay = fireDelay;
	}

	public void Update (Vector2 basePosition, Vector2 direction, float deltaTime)
	{
		if (direction == Vector2.zero) position = basePosition + offset;
		else position = basePosition + (Vector2)(Quaternion.Euler (0, 0, Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg) * offset);

		// Even if we dont have a target, tick the fire timer so we can fire as soon as we can again
		if (fireTimer < fireDelay) fireTimer += deltaTime;

		if (currentTarget != null && !currentTarget.isAlive) currentTarget = null;

		// Try to find a target if we dont have one, or the current target is out of range
		if (currentTarget == null || (currentTarget.targetPosition - position).sqrMagnitude > range * range)
		{
			findTarget ();
		}
		else
		{
			if (!playerAssignedTarget)
			{
				if (resetTargetTimer < resetTargetDelay) resetTargetTimer += deltaTime;
				else findTarget ();
			}
		}

		// We might now have a target, even if we didn't before, although we also might not
		if (currentTarget != null)
		{
			if (fireTimer >= fireDelay)
			{
				fireTimer -= fireDelay;
				fire ();
			}
		}

		OnUpdate (deltaTime);
	}

	public void TryTarget (Vector2 targetPosition)
	{
		float rangeSqr = targetLocationDetectionRange * targetLocationDetectionRange;

		// If the target is out of range, then we wont be able to target anthing
		if ((targetPosition - position).sqrMagnitude > rangeSqr) return;

		List<Ship> validTargets = map.agentManager.GetAgentsInRange<Ship> (targetPosition, targetLocationDetectionRange, (ship) =>
		{
			return ship.team != team && (ship.position - position).sqrMagnitude < rangeSqr;
		});

		if (validTargets.Count > 0)
		{
			// The player is saying to target this
			currentTarget = pickBestTarget (validTargets);
			playerAssignedTarget = true;
			resetTargetTimer = 0;
		}
	}

	private void fire ()
	{
		FireAtTaget (currentTarget);
	}

	private void findTarget ()
	{
		List<Ship> validShipTargets = map.agentManager.GetAgentsInRange<Ship> (position, range, (ship) =>
		{
			return ship.team != team;
		});

		if (validShipTargets.Count > 0)
		{
			// The player is saying to target this
			currentTarget = pickBestTarget (validShipTargets);
			playerAssignedTarget = false;
		}
		else
		{
			// Try target buildings

			List<Building> validBuildingTargets = map.GetBuildingsInRange (position, range, (building) =>
			{
				return building.team != team;
			});

			if (validBuildingTargets.Count > 0)
			{
				// The player is saying to target this
				currentTarget = pickBestTarget (validBuildingTargets);
				playerAssignedTarget = false;
			}
			else
			{
				currentTarget = null;
			}
		}

		resetTargetTimer = 0;
	}

	private Ship pickBestTarget (List<Ship> validTargets)
	{
		Ship best = null;

		for (int i = 0; i < validTargets.Count; i++)
		{
			if (best == null || isBetterTarget (validTargets[i], best)) best = validTargets[i];
		}

		return best;
	}

	private Building pickBestTarget (List<Building> validTargets)
	{
		Building best = null;

		for (int i = 0; i < validTargets.Count; i++)
		{
			if (best == null || isBetterTarget (validTargets[i], best)) best = validTargets[i];
		}

		return best;
	}

	private bool isBetterTarget (Ship ship, Ship currentBest)
	{
		switch (targetMode)
		{
			case TurretTargetMode.Strongest:
				return ship.GetStrengthFactor () > currentBest.GetStrengthFactor ();
			case TurretTargetMode.Weakest:
				return ship.GetStrengthFactor () < currentBest.GetStrengthFactor ();
			case TurretTargetMode.FurthestFromEnd:
				return (map.playerCommandCenter.position - ship.position).sqrMagnitude > (map.playerCommandCenter.position - currentBest.position).sqrMagnitude;
			case TurretTargetMode.ClosestToEnd:
			default:
				return (map.playerCommandCenter.position - ship.position).sqrMagnitude < (map.playerCommandCenter.position - currentBest.position).sqrMagnitude;
		}
	}

	private bool isBetterTarget (Building building, Building currentBest)
	{
		switch (targetMode)
		{
			case TurretTargetMode.Strongest:
				return building.GetStrengthFactor () > currentBest.GetStrengthFactor ();
			case TurretTargetMode.Weakest:
				return building.GetStrengthFactor () < currentBest.GetStrengthFactor ();
			case TurretTargetMode.FurthestFromEnd:
				return (map.playerCommandCenter.position - building.position).sqrMagnitude > (map.playerCommandCenter.position - currentBest.position).sqrMagnitude;
			case TurretTargetMode.ClosestToEnd:
			default:
				return (map.playerCommandCenter.position - building.position).sqrMagnitude < (map.playerCommandCenter.position - currentBest.position).sqrMagnitude;
		}
	}

	public virtual void OnDestroy () {}
	protected virtual void OnUpdate (float deltaTime) { }
	protected abstract void FireAtTaget (ITargetable target);
}
