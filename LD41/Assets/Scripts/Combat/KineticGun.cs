﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KineticGun : Gun
{
	public KineticGun (Map map, Team team, Vector2 offset, float range, float fireDelay) : base (map, team, offset, range, fireDelay) { }

	protected override void FireAtTaget (ITargetable target)
	{
		Vector2 direction = target.targetPosition - position;
		//float time = direction.magnitude / 6;
		Defines.fireBullet (map, team, position, direction);// + (target.velocity * time));
	}
}
