﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : Projectile
{
	public readonly float size;

	public override float CollisionTestRange
	{
		get
		{
			return size * 2f;
		}
	}

	public override float collisionSize
	{
		get
		{
			return size;
		}
	}

	public Missile (Map map, Team team, Vector2 position, Vector2 velocity, int damage, float splashDamageRange, float size, Mesh mesh, Material material)
		: base (map, team, position, velocity, damage, splashDamageRange, mesh, material)
	{
		this.size = size;
	}

	public override bool DoesCollide (Agent agent, float deltaTime, ref Vector2 positionDelta)
	{
		float maxDist = size + agent.collisionRadius;
		float dist = (agent.position - position).sqrMagnitude;
		return dist < maxDist * maxDist;
	}

	public override void OnDrawGizmos ()
	{
		Gizmos.color = Color.white;
		Gizmos.DrawWireSphere (position, size);
	}
}
