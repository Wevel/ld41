﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileGun : Gun
{
	public MissileGun (Map map, Team team, Vector2 offset, float range, float fireDelay) : base (map, team, offset, range, fireDelay) { }

	protected override void FireAtTaget (ITargetable target)
	{
		Vector2 direction = target.targetPosition - position;
		//float time = direction.magnitude / 2;
		Defines.fireMissile (map, team, position, direction);// + (target.velocity * time));
	}
}
