﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Projectile : ICollidable
{
	public readonly Map map;
	public readonly Team team;
	public Vector2 position { get; private set; }
	public readonly Vector2 velocity;
	public readonly int damage;
	public readonly float splashDamageRange;

	public abstract float CollisionTestRange
	{
		get;
	}

	public bool CanDamage
	{
		get
		{
			return !destroyed;
		}
	}

	private ProjectileDisplay display;
	private bool destroyed = false;

	public abstract float collisionSize { get; }

	protected Projectile (Map map, Team team, Vector2 position, Vector2 velocity, int damage, float splashDamageRange, Mesh mesh, Material material)
	{
		this.map = map;
		this.team = team;
		this.position = position;
		this.velocity = velocity;
		this.damage = damage;
		this.splashDamageRange = splashDamageRange;

		map.AddCollidable (this);

		ProjectileDisplay.AddDisplay (this, mesh, material);
	}

	public void Destroy ()
	{
		map.RemoveCollidable (this);
		display.SetProjectile (null);
		destroyed = true;
	}

	public void Update (float deltaTime)
	{
		position += velocity * deltaTime;

		// Check building collision
		foreach (Building item in map.Buildings)
		{
			if (Map.DoesRectIntersectCircle (item.collisionRectangle, position, collisionSize)) item.OnCollisionStay (this);
		}

		// Despawn bullets that have fallen off the map
		if (!map.OnMap (position, true)) Destroy ();
	}

	public int GetDamage (Vector2 target, bool isOrigionalHit)
	{
		if (isOrigionalHit) return damage;

		float distance = Vector2.Distance (target, position);

		if (distance < splashDamageRange / 2f) return damage;

		return Mathf.Clamp (Mathf.CeilToInt ((distance - (splashDamageRange / 2f)) / splashDamageRange), 0, damage);
	}

	public void SetDisplay (ProjectileDisplay display)
	{
		if (this.display != null) this.display.SetProjectile (null);
		if (display != null) display.SetProjectile (this);
		this.display = display;
	}

	public void OnCollisionStay (ICollidable other) { }

	public abstract bool DoesCollide (Agent agent, float deltaTime, ref Vector2 positionDelta);

	public virtual void OnDrawGizmos ()
	{
		Gizmos.color = Color.white;
		Gizmos.DrawWireSphere (position, 0.05f);
	}
}
