﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser
{
	private static Transform laserHolder;

	static Laser ()
	{
		laserHolder = new GameObject ("LaserHolder").transform;
	}

	private static Queue<MeshFilter> unusedDisplayes = new Queue<MeshFilter> ();

	public float damageDelay;
	public float width;
	public Material material;

	private MeshFilter display;
	private float damageTimer = 0;

	private float hideTimer = 0;

	public Laser (Material material, float damageDelay, float width)
	{
		this.damageDelay = damageDelay;
		this.width = width;
		this.material = material;
		display = getDisplay ();
	}

	public void Update (float deltaTime)
	{
		if (damageTimer < damageDelay) damageTimer += deltaTime;

		if (hideTimer > 0) hideTimer -= deltaTime;
		else display.gameObject.SetActive (false);
	}

	public void Destroy ()
	{
		if (display != null)
		{
			display.gameObject.SetActive (false);
			unusedDisplayes.Enqueue (display);
		}
	}

	public void Fire (Vector2 position, ITargetable target)
	{
		while (damageTimer >= damageDelay)
		{
			damageTimer -= damageDelay;
			target.DoDamage (1);
		}

		if (display == null) display = getDisplay ();

		display.transform.position = position;
		display.transform.rotation = Quaternion.Euler (0, 0, Mathf.Atan2 (target.targetPosition.y - position.y, target.targetPosition.x - position.x) * Mathf.Rad2Deg);

		float length = (target.targetPosition - position).magnitude;

		setmesh (length);

		display.gameObject.SetActive (true);
		hideTimer = 0.2f;
	}

	private MeshFilter getDisplay ()
	{
		if (unusedDisplayes.Count > 0) return unusedDisplayes.Dequeue ();

		GameObject go = new GameObject ("laser");
		go.transform.SetParent (laserHolder);

		MeshRenderer mr = go.AddComponent<MeshRenderer> ();
		mr.sharedMaterial = material;

		MeshFilter mf = go.AddComponent<MeshFilter> ();

		return mf;
	}

	private void setmesh (float length)
	{
		Mesh mesh = new Mesh ();

		mesh.vertices = new Vector3[]
		{
			new Vector3(0, width / 2f, -3),
			new Vector3(length, width / 2f, -3),
			new Vector3(length, -width / 2f, -3),
			new Vector3(0, -width / 2f, -3),
		};

		mesh.uv = new Vector2[]
		{
			new Vector3(0, 1, 0),
			new Vector3(1, 1, 0),
			new Vector3(1, 0, 0),
			new Vector3(0, 0, 0),
		};

		mesh.normals = new Vector3[]
		{
			new Vector3(0, 0, 1),
			new Vector3(0, 0, 1),
			new Vector3(0, 0, 1),
			new Vector3(0, 0, 1),
		};

		mesh.triangles = new int[]
		{
			0, 1, 2,
			0, 2, 3
		};


		if (display.mesh != null) Object.Destroy (display.mesh);
		display.mesh = mesh;
	}
}
