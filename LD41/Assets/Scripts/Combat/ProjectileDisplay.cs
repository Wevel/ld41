﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileDisplay : MonoBehaviour
{
	private static Transform holder;
	private static Queue<ProjectileDisplay> unusedDisplayes = new Queue<ProjectileDisplay> ();

	static ProjectileDisplay ()
	{
		holder = new GameObject ("ProjectileDisplayHolder").transform;
	}

	public static void AddDisplay (Projectile projectile, Mesh mesh, Material material)
	{
		ProjectileDisplay display;

		if (unusedDisplayes.Count > 0)
		{
			display = unusedDisplayes.Dequeue ();
			display.gameObject.SetActive (true);
		}
		else
		{
			GameObject go = new GameObject ("ProjectileDisplay");
			go.transform.SetParent (holder);
			go.AddComponent<MeshFilter> ();
			go.AddComponent<MeshRenderer> ();
			display = go.AddComponent<ProjectileDisplay> ();
		}

		display.mesh = mesh;
		display.material = material;

		projectile.SetDisplay (display);
	}

	private static void stopUsingDisplay (ProjectileDisplay display)
	{
		display.gameObject.SetActive (false);
		unusedDisplayes.Enqueue (display);
	}

	public Mesh mesh;
	public Material material;

	private Projectile projectile = null;

	private MeshFilter mf;
	private MeshRenderer mr;

	private void Awake ()
	{
		mf = this.GetComponent<MeshFilter> ();
		mr = this.GetComponent<MeshRenderer> ();
	}

	private void Update ()
	{
		if (projectile != null)
		{
			updatePosition ();
			updateDisplay ();
		}
	}

	public void SetProjectile (Projectile projectile)
	{
		this.projectile = projectile;

		if (projectile != null)
		{
			updatePosition ();
			updateDisplay ();
		}
		else
		{
			stopUsingDisplay (this);
		}
	}

	private void updatePosition ()
	{
		transform.position = projectile.position;
		transform.rotation = Quaternion.Euler (0, 0, Mathf.Atan2 (projectile.velocity.y, projectile.velocity.x) * Mathf.Rad2Deg);
	}

	private void updateDisplay ()
	{
		Material[] materials = new Material[mesh.subMeshCount];
		for (int i = 0; i < mesh.subMeshCount; i++) materials[i] = material;
		mr.sharedMaterials = materials;

		if (projectile == null) mf.mesh = null;
		else mf.mesh = mesh;
	}
}
