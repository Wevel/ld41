﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserGun : Gun
{
	private Laser laser;

	public LaserGun (Map map, Team team, Vector2 offset, Material laserMaterial, float laserFireDelay, float laserWidth, float range, float fireDelay) : base (map, team, offset, range, fireDelay)
	{
		laser = new Laser (laserMaterial, laserFireDelay, laserWidth);
	}

	protected override void OnUpdate (float deltaTime)
	{
		laser.Update (deltaTime);
	}

	public override void OnDestroy ()
	{
		laser.Destroy ();
	}

	protected override void FireAtTaget (ITargetable target)
	{
		laser.Fire (position, target);
	}
}
