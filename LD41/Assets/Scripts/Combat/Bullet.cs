﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Projectile
{
	public override float CollisionTestRange
	{
		get
		{
			return 0.1f;
		}
	}

	public override float collisionSize
	{
		get
		{
			return 0;
		}
	}

	public Bullet (Map map, Team team, Vector2 position, Vector2 velocity, int damage, Mesh mesh, Material material) : base (map, team, position, velocity, damage, 0, mesh, material) { }

	public override bool DoesCollide (Agent agent, float deltaTime, ref Vector2 positionDelta)
	{
		return (agent.position - position).sqrMagnitude < agent.collisionRadius * agent.collisionRadius;
	}
}
