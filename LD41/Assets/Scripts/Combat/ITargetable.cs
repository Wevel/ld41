﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITargetable
{
	bool isAlive { get; }
	Vector2 targetPosition { get; }
	Vector2 velocity { get; }
	Vector2 GetInRangePosition (Vector2 otherPosition, float range);
	void DoDamage (int amount);
}
