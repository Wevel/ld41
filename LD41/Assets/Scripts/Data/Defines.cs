﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defines : MonoBehaviour
{
	public delegate void FireProjectile (Map map, Team team, Vector2 position, Vector2 direction);

	[Header ("Ship Meshes")]
	public Mesh workerMesh;
	public Mesh minerMesh;
	public Mesh scoutMesh;
	public Mesh frigateMesh;
	public Mesh cruiserMesh;
	public Mesh dreadnoughtMesh;

	[Header ("Ship Materials")]
	public Material shipMaterial;

	[Header ("Ship icons")]
	public Sprite workerIcon;
	public Sprite minerIcon;
	public Sprite scoutIcon;
	public Sprite frigateIcon;
	public Sprite cruiserIcon;
	public Sprite dreadnoughtIcon;

	[Header ("Building Meshes")]
	public Mesh commandCenterMesh;
	public Mesh resourceDepoMesh;
	public Mesh hangerMesh;
	public Mesh smallShipyardMesh;
	public Mesh largeShipyardMesh;
	public Mesh kineticTurretMesh;
	public Mesh missileTurretMesh;
	public Mesh laserTurretMesh;

	[Header ("Building Materials")]
	public Material turretMaterial;
	public Material hangerMaterial;
	public Material smallShipYardMaterial;
	public Material largeShipYardMaterial;
	public Material commandCentreMaterial;
	public Material resourceDepoMaterial;

	[Header ("Building icons")]
	public Sprite commandCenterIcon;
	public Sprite hangarIcon;
	public Sprite resourceDepoIcon;
	public Sprite smallShipyardIcon;
	public Sprite largeShipyardIcon;
	public Sprite kineticTurretIcon;
	public Sprite missileTurretIcon;
	public Sprite laserTurretIcon;

	[Header ("Weapon Displays")]
	public Mesh bulletMesh;
	public Material bulletMaterial;
	public Mesh missileMesh;
	public Material missileMaterial;
	public Material laserMaterial;

	private static List<ShipType> shipTypes = new List<ShipType> ();
	private static List<BuildingType> buildingTypes = new List<BuildingType> ();

	public static FireProjectile fireBullet { get; private set; }
	public static FireProjectile fireMissile { get; private set; }

	public static ShipType GetShipType (string name)
	{
		return shipTypes.Find (x => x.name == name);
	}

	public static BuildingType GetBuildingType (string name)
	{
		return buildingTypes.Find (x => x.name == name);
	}

	private void Awake ()
	{
		DontDestroyOnLoad (gameObject);
		init ();
	}

	private void init ()
	{
		const float laserThickness = 0.02f;
		const float laserRange = 8;
		const float missileRange = 6;
		const float kineticRange = 5;
		const float missileFireRate = 1.5f;

		#region Ship Types

		#region Civilian Ships

		shipTypes.Add (new ShipType (
			"Worker",
			10,
			0.25f,
			1,
			0, 20, 40,
			75,
			workerIcon,
			(map, team, type, position, direction) => 
			{
				Ship ship = new Worker (map, team, type, position, direction, 20);
				AgentDisplay.AddDisplay (ship, workerMesh, shipMaterial);

				return ship;
			}));

		shipTypes.Add (new ShipType (
			"Miner",
			15,
			0.4f,
			1,
			0, 0, 50,
			75,
			minerIcon,
			(map, team, type, position, direction) =>
			{
				Ship ship = new Miner (map, team, type, position, direction, 1, 15);
				AgentDisplay.AddDisplay (ship, minerMesh, shipMaterial);

				return ship;
			})
		{
			collisionOffset = new Vector2(0.075f, 0),
		});

		#endregion
		#region Combat Ship

		//new KineticGun (map, team, new Vector2 (0, 0), 10, 0.3f)
		//new MissileGun (map, team, new Vector2 (0, 0), 8, 0.5f)
		//new LaserGun (map, team, new Vector2 (0, 0), laserMaterial, 0.03f, laserThickness, 15, 0))

		shipTypes.Add (new ShipType (
			"Scout",
			15,
			0.23f,
			1,
			30, 0, 50,
			50,
			scoutIcon,
			(map, team, type, position, direction) =>
			{
				Ship ship = new CombatShip (map, team, type, position, direction, 5,
					new KineticGun (map, team, new Vector2 (0, 0), kineticRange, 0.3f));
				AgentDisplay.AddDisplay (ship, scoutMesh, shipMaterial);

				return ship;
			})
		{
			collisionOffset = new Vector2 (-0.05f, 0),
		});

		shipTypes.Add (new ShipType (
			"Frigate",
			20,
			0.34f,
			2,
			50, 50, 20,
			250,
			frigateIcon,
			(map, team, type, position, direction) =>
			{
				Ship ship = new CombatShip (map, team, type, position, direction, 10,
					new KineticGun (map, team, new Vector2 (0.1f, 0), kineticRange, 0.3f),
					new MissileGun (map, team, new Vector2 (-0.1f, 0), missileRange, missileFireRate));
				AgentDisplay.AddDisplay (ship, frigateMesh, shipMaterial);

				return ship;
			}));

		shipTypes.Add (new ShipType (
			"Cruiser",
			30,
			0.45f,
			3,
			100, 50, 100,
			400,
			cruiserIcon,
			(map, team, type, position, direction) =>
			{
				Ship ship = new CombatShip (map, team, type, position, direction, 15,
					new LaserGun (map, team, new Vector2 (0, 0.1f), laserMaterial, 0.03f, laserThickness, laserRange, 0),
					new LaserGun (map, team, new Vector2 (0, -0.1f), laserMaterial, 0.03f, laserThickness, laserRange, 0),
					new KineticGun (map, team, new Vector2 (0.1f, 0), kineticRange, 0.3f),
					new KineticGun (map, team, new Vector2 (-0.1f, 0), kineticRange, 0.3f));
				AgentDisplay.AddDisplay (ship, cruiserMesh, shipMaterial);

				return ship;
			})
		{
			collisionOffset = new Vector2 (-1.341f, 0),
		});

		shipTypes.Add (new ShipType (
			"Dreadnought",
			60,
			0.57f,
			5,
			200, 200, 200,
			750,
			dreadnoughtIcon,
			(map, team, type, position, direction) =>
			{
				Ship ship = new CombatShip (map, team, type, position, direction, 10,
					new LaserGun (map, team, new Vector2 (0, 0.3f), laserMaterial, 0.03f, laserThickness, laserRange, 0),
					new LaserGun (map, team, new Vector2 (0, -0.3f), laserMaterial, 0.03f, laserThickness, laserRange, 0),
					new MissileGun (map, team, new Vector2 (0.1f, 0.3f), missileRange, missileFireRate),
					new MissileGun (map, team, new Vector2 (0.1f, -0.3f), missileRange, missileFireRate),
					new KineticGun (map, team, new Vector2 (-0.1f, 0.3f), kineticRange, 0.3f),
					new KineticGun (map, team, new Vector2 (-0.1f, -0.3f), kineticRange, 0.3f));
				AgentDisplay.AddDisplay (ship, dreadnoughtMesh, shipMaterial);

				return ship;
			})
		{
			collisionOffset = new Vector2 (-0.05f, 0),
		});

		#endregion

		#endregion
		#region Building Types

		buildingTypes.Add (new BuildingType (
			"Command Center",
			2000,
			new Vector2(3.4f, 3.4f),
			10,
			commandCenterIcon,
			commandCenterMesh, commandCentreMaterial,
			99999, 99999, 99999,
			(map, team, position, type) =>
			{
				Building building = new CommandCenter (map, team, position, type);
				BuildingDisplay.AddDisplay (building, commandCenterMesh, commandCentreMaterial);

				return building;
			}));

		buildingTypes.Add (new BuildingType (
			"Resource Depo",
			750,
			new Vector2 (2.1f, 2.1f),
			0,
			resourceDepoIcon,
			resourceDepoMesh, resourceDepoMaterial,
			0, 75, 0,
			(map, team, position, type) =>
			{
				Building building = new ResourceDepo (map, team, position, type);
				BuildingDisplay.AddDisplay (building, resourceDepoMesh, resourceDepoMaterial);

				return building;
			}));

		buildingTypes.Add (new BuildingType (
			"Hangar",
			500,
			new Vector2 (1.35f, 1.35f),
			10,
			hangarIcon,
			hangerMesh, hangerMaterial,
			15, 60, 0,
			(map, team, position, type) =>
			{
				Building building = new Hangar (map, team, position, type);
				BuildingDisplay.AddDisplay (building, hangerMesh, hangerMaterial);

				return building;
			}));

		buildingTypes.Add (new BuildingType (
			"Small Shipyard",
			1000,
			new Vector2 (1.9f, 2f),
			0,
			smallShipyardIcon,
			smallShipyardMesh, smallShipYardMaterial,
			0, 100, 100,
			(map, team, position, type) =>
			{
				Building building = new SmallShipYard (map, team, position, type);
				BuildingDisplay.AddDisplay (building, smallShipyardMesh, smallShipYardMaterial);

				return building;
			}));

		buildingTypes.Add (new BuildingType (
			"Large Shipyard",
			2500,
			new Vector2 (3.1f, 3.1f),
			0,
			largeShipyardIcon,
			largeShipyardMesh, largeShipYardMaterial,
			200, 400, 400,
			(map, team, position, type) =>
			{
				Building building = new LargeShipYard (map, team, position, type);
				BuildingDisplay.AddDisplay (building, largeShipyardMesh, largeShipYardMaterial);

				return building;
			}));

		buildingTypes.Add (new BuildingType (
			"Kinetic Turret",
			750,
			new Vector2 (1.2f, 1.2f),
			0,
			kineticTurretIcon,
			kineticTurretMesh, turretMaterial,
			100, 50, 50,
			(map, team, position, type) =>
			{
				Turret building = new Turret (map, team, position, type,
					new KineticGun (map, team, Vector2.zero, kineticRange, 0.3f));
				BuildingDisplay.AddDisplay (building, kineticTurretMesh, turretMaterial);

				return building;
			}));

		buildingTypes.Add (new BuildingType (
			"Missile Turret",
			1250,
			new Vector2 (1.2f, 1.2f),
			0,
			missileTurretIcon,
			missileTurretMesh, turretMaterial,
			100, 150, 100,
			(map, team, position, type) =>
			{
				Turret building = new Turret (map, team, position, type,
					new MissileGun (map, team, Vector2.zero, missileRange, missileFireRate));
				BuildingDisplay.AddDisplay (building, missileTurretMesh, turretMaterial);

				return building;
			}));

		buildingTypes.Add (new BuildingType (
			"Laser Turret",
			1750,
			new Vector2 (1.2f, 1.2f),
			0,
			laserTurretIcon,
			laserTurretMesh, turretMaterial,
			200, 150, 50,
			(map, team, position, type) =>
			{
				Turret building = new Turret (map, team, position, type,
					new LaserGun (map, team, Vector2.zero, laserMaterial, 0.03f, laserThickness, laserRange, 0));
				BuildingDisplay.AddDisplay (building, laserTurretMesh, turretMaterial);

				return building;
			}));

		#endregion
		#region Projectiles

		fireBullet = (map, team, position, direction) =>
		{
			new Bullet (map, team, position, direction.normalized * 6, 5, bulletMesh, bulletMaterial);
		};

		fireMissile = (map, team, position, direction) =>
		{
			new Missile (map, team, position, direction.normalized * 2, 10, 1.5f, 0.175f, missileMesh, missileMaterial);
		};

		#endregion
	}
}
