﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ResourceType
{
	None = 0,
	Water,
	Aluminium,
	Platinum
}
