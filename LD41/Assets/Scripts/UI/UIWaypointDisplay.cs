﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWaypointDisplay : MonoBehaviour
{
	public Transform waypointPrefab;

	private Queue<Transform> waypointQueue = new Queue<Transform> ();
	private List<Transform> currentWaypoints = new List<Transform> ();

	public void Show (List<Vector2> points)
	{
		Hide ();
		Transform t;

		for (int i = 0; i < points.Count; i++)
		{
			t = getWaypoint ();
			t.gameObject.SetActive (true);
			t.localPosition = points[i];
			currentWaypoints.Add (t);
		}
	}

	public void Hide ()
	{
		for (int i = 0; i < currentWaypoints.Count; i++)
		{
			currentWaypoints[i].gameObject.SetActive (false);
			waypointQueue.Enqueue (currentWaypoints[i]);
		}

		currentWaypoints.Clear ();
	}

	private Transform getWaypoint ()
	{
		if (waypointQueue.Count > 0) return waypointQueue.Dequeue ();
		else return Instantiate (waypointPrefab, transform);
	}

}
