﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMainMenu : MonoBehaviour
{
	public Game game;

	public GameObject gameOverMenu;

	public GameObject mainUIObject;
	public GameObject controlGroupsObject;
	public GameObject mapBoarder;

	public GameObject mainMenuObject;
	public GameObject startGameButton;
	public GameObject restartGameButton;
	public GameObject resumeGameButton;

	public UIShipControl shipControl;

	public Text nextWaveTimerText;
	public Text currentWaveNumber;
	public GameObject waveUIObject;

	private void Update ()
	{
		if (game.map != null)
		{
			currentWaveNumber.text = "Wave: " + game.currentWaveCount;
			nextWaveTimerText.text = "Next wave in: " + game.nextWaveTimer.ToString ("f0");
		}

		waveUIObject.SetActive (game.map != null && !game.gameOver);

		mainUIObject.SetActive (game.map != null);
		controlGroupsObject.SetActive (game.map != null);
		mapBoarder.SetActive (game.map != null);

		mainMenuObject.SetActive ((game.paused && !game.gameOver) || game.map == null);
		startGameButton.SetActive (game.map == null);
		restartGameButton.SetActive (game.map != null);
		resumeGameButton.SetActive (game.map != null);

		if (Input.GetKeyDown (KeyCode.Escape)) game.paused = !game.paused;

		gameOverMenu.SetActive (game.map != null && game.gameOver);
	}

	public void PauseGame ()
	{
		game.paused = true;
	}

	public void ResumeGame ()
	{
		game.paused = false;
	}

	public void StartGame ()
	{
		game.StartGame ();
		shipControl.Clear ();
	}

	public void ExitGame ()
	{
		Application.Quit ();
	}
}