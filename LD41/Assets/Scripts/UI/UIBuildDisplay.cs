﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBuildDisplay : MonoBehaviour
{
	private MeshFilter mf;
	private MeshRenderer mr;

	private Material currentMaterial;

	private void Awake ()
	{
		mf = this.GetComponent<MeshFilter> ();
		mr = this.GetComponent<MeshRenderer> ();
	}

	private void OnDestroy ()
	{
		if (currentMaterial != null) Destroy (currentMaterial);
	}

	public void UpdateState (Vector2 position, Color colour)
	{
		currentMaterial.color = colour;
		transform.position = position;
	}

	public void Show (Mesh mesh, Material material)
	{
		gameObject.SetActive (true);

		mf.sharedMesh = mesh;

		if (currentMaterial != null) Destroy (currentMaterial);
		currentMaterial = new Material (material);

		if (currentMaterial.HasProperty ("_BuiltPercent")) currentMaterial.SetFloat ("_BuiltPercent", 1f);
		if (currentMaterial.HasProperty ("_Offset")) currentMaterial.SetVector ("_Offset", Vector4.zero);

		 Material[] materials = new Material[mesh.subMeshCount];
		for (int i = 0; i < mesh.subMeshCount; i++) materials[i] = currentMaterial;
		mr.materials = materials;

	}

	public void Hide ()
	{
		gameObject.SetActive (false);
	}
}
