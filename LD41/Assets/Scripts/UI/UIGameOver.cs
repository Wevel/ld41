﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameOver : MonoBehaviour
{
	public Game game;
	public Text waverCounter;
	
	void Update () {
		if (game != null)
		{
			waverCounter.text = "You made it to wave: " + game.currentWaveCount;
		}
	}
}
