﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMainSelectedItem : MonoBehaviour
{
	public GameObject displayHolder;
	public Text nameText;
	public Text healthText;
	public Image iconImage;
	public Transform healthBar;

	private ISelectable selected;

	public void SelectItem (ISelectable item)
	{
		selected = item;

		Update ();
	}

	private void Update ()
	{
		displayHolder.SetActive (selected != null);

		if (selected != null)
		{
			nameText.text = selected.name;
			healthText.text = string.Format ("{0}/{1}", selected.currentHealth, selected.maxHealth);
			iconImage.sprite = selected.icon;
			healthBar.localScale = new Vector3 (Mathf.Clamp01 (selected.currentHealth / (float)selected.maxHealth), 1, 1);
		}
	}
}
