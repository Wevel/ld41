﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISelectableStatus : MonoBehaviour
{
	public delegate void OnCancelClicked ();

	public Image iconImage;
	public Text countText;

	private OnCancelClicked onCancel;

	public void ShowResourceCount (Sprite icon, int count, OnCancelClicked onCancel)
	{
		iconImage.sprite = icon;
		if (count != 1) countText.text = count + "";
		else countText.text = "";
		this.onCancel = onCancel;
	}

	public void ShowSpawnCount (Sprite icon, int count, OnCancelClicked onCancel)
	{
		iconImage.sprite = icon;
		if (count != 1) countText.text = "X" + count;
		else countText.text = "";
		this.onCancel = onCancel;
	}

	public void ShowSpawnPercent (Sprite icon, int count, OnCancelClicked onCancel)
	{
		iconImage.sprite = icon;
		countText.text = count + "%";
		this.onCancel = onCancel;
	}

	public void Clicked ()
	{
		if (onCancel != null) onCancel ();
	}
}
