﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public Game game;

	public float minZoom = 2;
	public float maxZoom = 50;
	public float keyBoardPanSpeed = 5;
	public float keyZoomSpeed = 2;
	public float zoomSpeed = 1;

	private Vector2 lastPosition;

	private Rect limitRect;
	private bool useLimitRect = false;

	private bool isDoingDrag = false;

	void Update ()
	{
		if (game != null && game.map != null)
		{
			if (game.paused || game.gameOver) return;

			Camera mainCamera = Camera.main;
			if (mainCamera != null)
			{
				// Mouse movement
				Vector2 currentPos = mainCamera.ScreenToWorldPoint (Input.mousePosition);
				if (mainCamera.pixelRect.Contains (Input.mousePosition) && Input.GetMouseButtonDown (2))
				{
					lastPosition = currentPos;
					isDoingDrag = true;
				}

				if (isDoingDrag && Input.GetMouseButton (2))
				{
					mainCamera.transform.Translate (lastPosition - currentPos, Space.World);
				}
				else
				{
					isDoingDrag = false;

					zoom (mainCamera, currentPos, Input.GetAxis ("Mouse ScrollWheel"));
					mainCamera.orthographicSize -= mainCamera.orthographicSize * Input.GetAxis ("Mouse ScrollWheel") * zoomSpeed;

					if (mainCamera.orthographicSize <= minZoom) mainCamera.orthographicSize = minZoom;
					else if (mainCamera.orthographicSize >= maxZoom) mainCamera.orthographicSize = maxZoom;
					else mainCamera.transform.Translate ((currentPos - (Vector2)mainCamera.transform.position) * Input.GetAxis ("Mouse ScrollWheel"), Space.World);
				}

				if (isDoingDrag) lastPosition = mainCamera.ScreenToWorldPoint (Input.mousePosition);

				//Keyboard movement
				if (!Input.GetMouseButton (2))
				{
					mainCamera.transform.Translate (
						Input.GetAxis ("Horizontal") * keyBoardPanSpeed * mainCamera.orthographicSize * Time.deltaTime,
						Input.GetAxis ("Vertical") * keyBoardPanSpeed * mainCamera.orthographicSize * Time.deltaTime,
						0,
						Space.World);

					float zoomDirection = 0;
					if (Input.GetKey (KeyCode.Equals)) zoomDirection = 1;
					else if (Input.GetKey (KeyCode.Minus)) zoomDirection = -1;

					zoom (mainCamera, mainCamera.transform.position, zoomDirection * keyZoomSpeed * Time.deltaTime);
				}

				if (useLimitRect) limitToRect (mainCamera);
			}
		}
	}

	public void SetLocation (Vector2 position)
	{
		Camera mainCamera = Camera.main;

		Vector3 pos = mainCamera.transform.position;
		pos.x = position.x;
		pos.y = position.y;
		mainCamera.transform.position = pos;

		if (useLimitRect) limitToRect (mainCamera);
	}

	public void SetLimitRectangle (Rect limitRect)
	{
		this.limitRect = limitRect;
		useLimitRect = true;
	}

	public void StopUsingLimitRectangle ()
	{
		useLimitRect = false;
	}

	private void zoom (Camera mainCamera, Vector2 currentPos, float direction)
	{
		mainCamera.orthographicSize -= mainCamera.orthographicSize * direction * zoomSpeed;

		if (mainCamera.orthographicSize <= minZoom) mainCamera.orthographicSize = minZoom;
		else if (mainCamera.orthographicSize >= maxZoom) mainCamera.orthographicSize = maxZoom;
		else mainCamera.transform.Translate ((currentPos - (Vector2)mainCamera.transform.position) * direction, Space.World);
	}

	private void limitToRect (Camera mainCamera)
	{
		Vector3 pos = mainCamera.transform.position;
		pos.x = Mathf.Clamp (pos.x, limitRect.xMin, limitRect.xMax);
		pos.y = Mathf.Clamp (pos.y, limitRect.yMin, limitRect.yMax);
		mainCamera.transform.position = pos;
	}
}
