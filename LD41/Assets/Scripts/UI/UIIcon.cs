﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIIcon : MonoBehaviour
{
	public UIShipControl shipControl;

	public Image iconImage;
	public Text countText;
	public Text nameText;

	public void SetIcon (UIShipControl shipControl, Sprite icon, string name, int count)
	{
		this.shipControl = shipControl;
		iconImage.sprite = icon;
		nameText.text = name;
		countText.text = "X" + count;
	}

	public void Clicked ()
	{
		shipControl.LimitSelection (nameText.text);
	}
}
