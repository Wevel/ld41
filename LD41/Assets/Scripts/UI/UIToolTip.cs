﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIToolTip : MonoBehaviour
{
	public GameObject toolTip;
	public Text toolTipText;

	public string text = "";
	public float toolTipDelay = 0.25f;

	private RectTransform rt;

	private float toolTipTimer = 0;

	private void Awake ()
	{
		rt = this.GetComponent<RectTransform> ();
	}

	private void LateUpdate ()
	{
		toolTipText.text = text;

		Rect r = new Rect (rt.position, rt.sizeDelta);
		if (r.Contains (Input.mousePosition))
		{
			if (toolTipTimer < toolTipDelay) toolTipTimer += Time.deltaTime;
			else toolTip.gameObject.SetActive (true);
		}
		else
		{
			toolTipTimer = 0;
			toolTip.gameObject.SetActive (false);
		}
	}
}
