﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIShipControl : MonoBehaviour
{
	public Game game;

	public RectTransform shipHighlightHolder;
	public RectTransform boxDragTransform;
	public RectTransform shipHighlightPrefab;
	public GameObject[] controlGroupButtons = new GameObject[9];
	public GameObject addControlGroupButton;
	public UIIcon uiIconPrefab;
	public UISelectableStatus statusIconPrefab;
	public Transform selectedIconsHolder;
	public Transform statusEffectsHolder;
	public Text repeatText;
	public GameObject repeatButton;
	public Color repeatButtonOffColour = Color.white;
	public Color repeatButtonOnColour = Color.white;

	public Sprite platinumIcon;
	public Sprite aluminiumIcon;
	public Sprite waterIcon;

	public float selectedHighlightScale = 32;

	public UIMainSelectedItem mainSelectionItem;

	public UIBuildDisplay buildDisplay;

	public UIWaypointDisplay waypointsDisplay;

	[Header ("Action Menus")]
	public GameObject workerActionsBar;
	public GameObject comandCenterActionsBar;
	public GameObject smallShipyardActionsBar;
	public GameObject largeShipyardActionsBar;

	[Header ("Resources")]
	public Text platinumResrouceCountText;
	public Text aluminiumResrouceCountText;
	public Text waterResrouceCountText;
	public Text fleetUsageResrouceCountText;

	public Color cantBuildcolour;
	public Color noResourcesCantBuildcolour;

	private Queue<RectTransform> shipHighlightQueue = new Queue<RectTransform> ();
	private Dictionary<ISelectable, RectTransform> highlightedItems = new Dictionary<ISelectable, RectTransform> ();
	private Dictionary<ISelectable, RectTransform> selectedItems = new Dictionary<ISelectable, RectTransform> ();

	private Queue<UIIcon> selectedIconsQueue = new Queue<UIIcon> ();
	private List<UIIcon> currentSelectedIcons = new List<UIIcon> ();

	private Queue<UISelectableStatus> statusIconsQueue = new Queue<UISelectableStatus> ();
	private List<UISelectableStatus> currentStatusIcons = new List<UISelectableStatus> ();

	private Vector2 dragStart;
	private bool isDoingDrag = false;
	private bool hasSelectedChanged = false;
	private Ship currentLeader = null;
	private BuildingType toBuildType = null;

	private List<ISelectable>[] controlGroups = new List<ISelectable>[9];

	private void Awake ()
	{
		Debug.Assert (controlGroups.Length == controlGroupButtons.Length, "Don't have " + controlGroups.Length + " control group buttons");

		for (int i = 0; i < controlGroups.Length; i++) controlGroups[i] = new List<ISelectable> ();

		buildDisplay.Hide ();
	}

	private void Update ()
	{
		Camera mainCamera = Camera.main;

		if (game != null && game.map != null)
		{
			if (game.paused || game.gameOver) return;

			platinumResrouceCountText.text = string.Format ("{0}", game.map.playerTeam.platinumCount);
			aluminiumResrouceCountText.text = string.Format ("{0}", game.map.playerTeam.aluminiumCount);
			waterResrouceCountText.text = string.Format ("{0}", game.map.playerTeam.waterCount);
			fleetUsageResrouceCountText.text = string.Format ("{0}/{1}", game.map.playerTeam.fleetUsage, game.map.playerTeam.fleetLimit);

			updateDragShips (mainCamera);
			comandShips (mainCamera);
			updateControlGroups ();
			updateBuildDisplay (mainCamera);
			updateSelectedUI ();

			if (isDoingDrag && !Input.GetMouseButton (0))
			{
				boxDragTransform.gameObject.SetActive (false);
				isDoingDrag = false;
			}

			List<ISelectable> selected = new List<ISelectable> (selectedItems.Keys);
			for (int i = 0; i < selected.Count; i++)
			{
				RectTransform rt;

				if (!selected[i].isAlive)
				{
					rt = selectedItems[selected[i]];
					rt.gameObject.SetActive (false);
					shipHighlightQueue.Enqueue (rt);

					selectedItems.Remove (selected[i]);
					hasSelectedChanged = true;
				}
			}

			//if (Input.GetMouseButtonDown (3))
			//{
			//	Defines.fireBullet (game.map, game.map.enemyTeam, mainCamera.ScreenToWorldPoint (Input.mousePosition), new Vector2 (-2, 0));
			//}
			//else if (Input.GetMouseButtonDown (4))
			//{
			//	Defines.fireMissile (game.map, game.map.enemyTeam, mainCamera.ScreenToWorldPoint (Input.mousePosition), new Vector2 (-2, 0));
			//}

		}
		else
		{
			Clear ();
		}

	}

	public void Clear ()
	{
		for (int i = 0; i < controlGroups.Length; i++) controlGroups[i].Clear ();
		clearSelectedShips ();
		clearSelectionIcons ();
		clearStatusIcons ();
		clearHighlightedShips ();
		waypointsDisplay.Hide ();
		isDoingDrag = false;
		hasSelectedChanged = false;
		currentLeader = null;
	}

	private void updateDragShips (Camera mainCamera)
	{
		if (!EventSystem.current.IsPointerOverGameObject ())
		{
			if (toBuildType == null)
			{
				if (mainCamera.pixelRect.Contains (Input.mousePosition))
				{
					if (Input.GetMouseButtonDown (0))
					{
						dragStart = clampToScreen (mainCamera, Input.mousePosition);

						boxDragTransform.gameObject.SetActive (true);
						isDoingDrag = true;
					}
				}

				if (isDoingDrag)
				{
					if (Input.GetMouseButtonUp (0))
					{
						Vector2 dragEnd = clampToScreen (mainCamera, Input.mousePosition);
						Rect dragRect = screenToWroldRect (mainCamera, new Rect (
							Mathf.Min (dragStart.x, dragEnd.x),
							Mathf.Min (dragStart.y, dragEnd.y),
							Mathf.Abs (dragStart.x - dragEnd.x),
							Mathf.Abs (dragStart.y - dragEnd.y)));

						List<ISelectable> shipsInRect = game.map.FindSelectableInRect (dragRect);

						boxDragTransform.gameObject.SetActive (false);
						isDoingDrag = false;

						clearHighlightedShips ();

						// If control isn't held down, clear the currently selected ships
						if (!addToExisiting (false)) clearSelectedShips ();

						// Add the new ships to the selection
						RectTransform rt;
						for (int i = 0; i < shipsInRect.Count; i++)
						{
							if (!selectedItems.ContainsKey (shipsInRect[i]))
							{
								rt = getShipHighlight ();
								rt.gameObject.SetActive (true);
								rt.sizeDelta = shipsInRect[i].boundsSize * selectedHighlightScale;

								selectedItems.Add (shipsInRect[i], rt);
								hasSelectedChanged = true;
							}
						}
					}
					else if (Input.GetMouseButton (0))
					{
						Vector2 dragEnd = clampToScreen (mainCamera, Input.mousePosition);
						Rect dragRect = new Rect (
							Mathf.Min (dragStart.x, dragEnd.x),
							Mathf.Min (dragStart.y, dragEnd.y),
							Mathf.Abs (dragStart.x - dragEnd.x),
							Mathf.Abs (dragStart.y - dragEnd.y));

						// Update the drag box
						boxDragTransform.position = dragRect.position;
						boxDragTransform.sizeDelta = dragRect.size;

						List<ISelectable> shipsInRect = game.map.FindSelectableInRect (screenToWroldRect (mainCamera, dragRect));

						// Update the highlighted ships
						// Clear the old highlights
						clearHighlightedShips ();

						RectTransform rt;
						// Add the new ones
						for (int i = 0; i < shipsInRect.Count; i++)
						{
							if (!selectedItems.ContainsKey (shipsInRect[i]))
							{
								rt = getShipHighlight ();
								rt.gameObject.SetActive (true);
								rt.sizeDelta = shipsInRect[i].boundsSize * selectedHighlightScale;

								highlightedItems.Add (shipsInRect[i], rt);
							}
						}
					}
				}
			}
			else
			{
				boxDragTransform.gameObject.SetActive (false);
			}
		}
	}

	private void comandShips (Camera mainCamera)
	{
		if (!EventSystem.current.IsPointerOverGameObject ())
		{
			if (Input.GetKeyDown (KeyCode.Delete))
			{
				ISelectable firstSelected = getFirstselectedItem ();
				if (!(firstSelected is CommandCenter))
				{
					RectTransform rt = selectedItems[firstSelected];

					rt.gameObject.SetActive (false);
					shipHighlightQueue.Enqueue (rt);

					selectedItems.Remove (firstSelected);

					firstSelected.Destroy ();
					hasSelectedChanged = true;
				}
			}
			else
			{
				// Make sure we arn't also doing a drag of some sort
				if (!Input.GetMouseButton (0) && Input.GetMouseButtonDown (1))
				{
					// Make sure we actually have some units selected
					if (selectedItems.Count > 0)
					{
						if (toBuildType != null)
						{
							Vector2 mousePosition = mainCamera.ScreenToWorldPoint (Input.mousePosition);

							if (game.map.playerTeam.CanMakeBuilding (toBuildType) 
								&& game.map.CanBuildBuilding (new Rect (mousePosition - (toBuildType.size / 2f), toBuildType.size)))
							{
								// Make the building
								toBuildType.spawnBuilding (game.map, game.map.playerTeam, mousePosition, toBuildType);

								// Tell all selected units to go to it
								commandToLocation (mainCamera);

								if (!addToExisiting (false)) toBuildType = null;
							}
						}
						else
						{
							commandToLocation (mainCamera);
						}
					}
				}
			}
		}
	}

	private void commandToLocation (Camera mainCamera)
	{
		ISelectable firstSelected = getFirstselectedItem ();
		if (firstSelected is Ship)
		{
			if (((Ship)firstSelected).team == game.map.playerTeam)
			{
				ICommand newCommand = new MoveCommand (mainCamera.ScreenToWorldPoint (Input.mousePosition), 1);

				List<ISelectable> atTarget = game.map.FindAllSelectableInRect (
					new Rect (mainCamera.ScreenToWorldPoint (Input.mousePosition) - new Vector3 (0.25f, 0.25f), new Vector2 (0.25f, 0.25f)));

				List<ISelectable> remainingSelectable = new List<ISelectable> (selectedItems.Keys);

				if (atTarget.Count > 0)
				{
					for (int i = remainingSelectable.Count - 1; i >= 0; i--)
					{
						if (remainingSelectable[i] is Miner)
						{
							Asteroid asteroid = (Asteroid)atTarget.Find (x => x is Asteroid && ((Asteroid)x).resourceType != ResourceType.None);
							if (asteroid != null)
							{
								giveCommand ((Ship)remainingSelectable[i], new MineCommand (asteroid));
								remainingSelectable.RemoveAt (i);
							}
						}
						else if (remainingSelectable[i] is Worker)
						{
							Building building = (Building)atTarget.Find (x => x is Building);
							if (building != null)
							{
								giveCommand ((Ship)remainingSelectable[i], new BuildCommand (building));
								remainingSelectable.RemoveAt (i);
							}
						}
						else if (remainingSelectable[i] is CombatShip)
						{
							ITargetable target = (ITargetable)atTarget.Find (x => x is ITargetable);
							if (target != null)
							{
								CombatShip ship = (CombatShip)remainingSelectable[i];
								giveCommand (ship, new AttackCommand (target, ship.attackRange));
								remainingSelectable.RemoveAt (i);
							}
						}
					}
				}

				if (newCommand != null && remainingSelectable.Count > 0)
				{
					// If we right click then we should control ships to do something
					if (hasSelectedChanged || currentLeader == null || currentLeader.stateMachine.IsInState<ShipStates.Idle> ())
					{
						List<Ship> ships = new List<Ship> ();
						for (int i = 0; i < remainingSelectable.Count; i++)
						{
							if (remainingSelectable[i] is Ship) ships.Add ((Ship)remainingSelectable[i]);
							else if (!(remainingSelectable[i] is Agent)) Debug.LogError ("Has a selection with things that arn't ships or other agents");
						}

						// There is either no leader or ships in the selection have changed then we need to re group the ships
						// Or there is a current leader, but it has gone to idle and so lost its group
						currentLeader = AgentManager.SetupGroupGridFormation (ships, 1, 1.78f);
						currentLeader.stateMachine.SetState (new ShipStates.FollowComandQueue (new CommandQueue (newCommand), 1f));
					}
					else
					{
						if (currentLeader.stateMachine.IsInState<ShipStates.FollowComandQueue> () && addToExisiting (false))
						{
							// The ships is already following a command queue, so add this to the end
							// For now we can only move, not attack things
							((ShipStates.FollowComandQueue)currentLeader.stateMachine.currentState).comandQueue.AddCommand (newCommand);
						}
						else
						{
							currentLeader.stateMachine.SetState (new ShipStates.FollowComandQueue (new CommandQueue (newCommand), 1f));
						}
					}
				}
			}
		}
		else
		{
			Debug.Assert (firstSelected is Building, "Selected item is not a ship or a building");

			UnitSpawner unitSpawner;
			Turret turret;

			Vector2 mousePosition = mainCamera.ScreenToWorldPoint (Input.mousePosition);

			foreach (KeyValuePair<ISelectable, RectTransform> item in selectedItems)
			{
				if (item.Key is UnitSpawner)
				{
					unitSpawner = (UnitSpawner)item.Key;
					unitSpawner.spawnTarget = mousePosition;
				}
				else if (item.Key is Turret)
				{
					turret = (Turret)item.Key;
					turret.TryTarget (mousePosition);
				}
			}
		}

		hasSelectedChanged = false;
	}

	private void giveCommand (Ship ship, ICommand command)
	{
		if (ship.stateMachine.IsInState<ShipStates.FollowComandQueue> ()) ((ShipStates.FollowComandQueue)ship.stateMachine.currentState).comandQueue.AddCommand (command);
		else ship.stateMachine.SetState (new ShipStates.FollowComandQueue (new CommandQueue (command), 1));
	}

	private void updateBuildDisplay (Camera mainCamera)
	{
		if (toBuildType != null)
		{
			if (!EventSystem.current.IsPointerOverGameObject ())
			{
				Vector2 mousePosition = mainCamera.ScreenToWorldPoint (Input.mousePosition);

				Color displayColour;

				if (game.map.CanBuildBuilding (new Rect (mousePosition - (toBuildType.size / 2f), toBuildType.size)))
				{
					if (game.map.playerTeam.CanMakeBuilding (toBuildType)) displayColour = Color.white;
					else displayColour = noResourcesCantBuildcolour;
				}
				else
				{
					displayColour = cantBuildcolour;
				}

				buildDisplay.Show (toBuildType.mesh, toBuildType.material);
				buildDisplay.UpdateState (mousePosition, displayColour);

				if (Input.GetMouseButtonDown (0) || Input.GetKeyDown (KeyCode.Escape))
				{
					toBuildType = null;
					buildDisplay.Hide ();
				}
			}
			else
			{
				buildDisplay.Hide ();
			}
		}
		else
		{
			buildDisplay.Hide ();
		}
	}

	private ISelectable getFirstselectedItem ()
	{
		foreach (KeyValuePair<ISelectable, RectTransform> item in selectedItems) return item.Key;
		return null;
	}

	private void updateControlGroups ()
	{
		// Make sure that the user isn't doing anything with the mouse
		if (!Input.GetMouseButton (0) && !Input.GetMouseButton (1))
		{
			SelectControlGroup (getControlGroupIndex ());			
		}

		bool isOneNotActive = false;
		for (int i = 0; i < controlGroups.Length; i++)
		{
			controlGroupButtons[i].SetActive (controlGroups[i].Count > 0);
			if (controlGroups[i].Count == 0) isOneNotActive = true;
		}

		addControlGroupButton.SetActive (isOneNotActive && selectedItems.Count > 0);
	}

	private void LateUpdate ()
	{
		Camera mainCamera = Camera.main;

		// Update the highlight positions
		foreach (KeyValuePair<ISelectable, RectTransform> item in selectedItems)
			item.Value.position = mainCamera.WorldToScreenPoint (item.Key.position);

		foreach (KeyValuePair<ISelectable, RectTransform> item in highlightedItems)
			item.Value.position = mainCamera.WorldToScreenPoint (item.Key.position);
	}

	private void OnGUI ()
	{
		Event e = Event.current;
		if (e.isKey && e.control)
		{
			int selectedIndex = getControlGroupIndex (e.keyCode);
			if (selectedIndex >= 0 && selectedIndex <= 9)
			{
				// Create control group
				addSelectedToControlGroup (selectedIndex);
			}
		}
	}

	public void AddControlGroup ()
	{
		int addIndex = -1;

		for (int i = 0; i < controlGroups.Length; i++)
		{
			if (controlGroups[i].Count == 0)
			{
				addIndex = i;
				break;
			}
		}

		if (addIndex >= 0)
		{
			addSelectedToControlGroup (addIndex);
		}
	}

	public void SelectControlGroup (int selectedIndex)
	{
		if (selectedIndex >= 0 && selectedIndex <= 9)
		{
			if (modifyInput (false))
			{
				// Create control group
				addSelectedToControlGroup (selectedIndex);
			}
			else if (addToExisiting (false))
			{
				// Select control group in addition to current units
				RectTransform rt;

				for (int i = 0; i < controlGroups[selectedIndex].Count; i++)
				{
					if (!selectedItems.ContainsKey (controlGroups[selectedIndex][i]))
					{
						rt = getShipHighlight ();
						rt.gameObject.SetActive (true);
						rt.sizeDelta = controlGroups[selectedIndex][i].boundsSize * selectedHighlightScale;

						selectedItems.Add (controlGroups[selectedIndex][i], rt);
						hasSelectedChanged = true;
					}
				}
			}
			else
			{
				// Select control group instead of current units

				if (controlGroups[selectedIndex].Count > 0)
				{
					clearSelectedShips ();

					RectTransform rt;

					for (int i = 0; i < controlGroups[selectedIndex].Count; i++)
					{
						if (!selectedItems.ContainsKey (controlGroups[selectedIndex][i]))
						{
							rt = getShipHighlight ();
							rt.gameObject.SetActive (true);
							rt.sizeDelta = controlGroups[selectedIndex][i].boundsSize * selectedHighlightScale;

							selectedItems.Add (controlGroups[selectedIndex][i], rt);
							hasSelectedChanged = true;
						}
					}
				}
			}
		}
	}

	public void QueueBuild (string type)
	{
		if (selectedItems.Count > 0)
		{
			ISelectable first = getFirstselectedItem ();

			if (first is Building)
			{
				foreach (KeyValuePair<ISelectable, RectTransform> item in selectedItems)
					item.Key.TryQueueBuild (type, Camera.main.ScreenToWorldPoint (Input.mousePosition));
			}
			else
			{
				// We are trying to make a building
				toBuildType = Defines.GetBuildingType (type);
				if (type == null) Debug.LogWarning ("No building with name: " + type);

				buildDisplay.Show (toBuildType.mesh, toBuildType.material);
				updateBuildDisplay (Camera.main);
			}
		}
	}

	private void updateSelectedUI ()
	{
		workerActionsBar.SetActive (false);
		comandCenterActionsBar.SetActive (false);
		smallShipyardActionsBar.SetActive (false);
		largeShipyardActionsBar.SetActive (false);

		ISelectable first = getFirstselectedItem ();

		mainSelectionItem.SelectItem (first);

		if (first != null)
		{
			if (first is Ship)
			{
				if (hasSelectionGotType<Worker> ()) workerActionsBar.SetActive (true);
			}
			else if (first is Building)
			{
				bool hasCommandCenter = hasSelectionGotType<CommandCenter> ();
				bool hasSmallShipyard = hasSelectionGotType<SmallShipYard> ();
				bool hasLargeShipyard = hasSelectionGotType<LargeShipYard> ();

				if (hasCommandCenter)
				{
					if (!hasSmallShipyard && !hasLargeShipyard)
						comandCenterActionsBar.SetActive (true);
				}
				else if (hasSmallShipyard)
				{
					if (!hasLargeShipyard) smallShipyardActionsBar.SetActive (true);
				}
				else if (hasLargeShipyard)
				{
					largeShipyardActionsBar.SetActive (true);
				}
			}
		}

		clearSelectionIcons ();

		Dictionary<string, int> iconsToMake = new Dictionary<string, int> ();
		Dictionary<string, Sprite> iconSpritesToMake = new Dictionary<string, Sprite> ();
		foreach (KeyValuePair<ISelectable, RectTransform> item in selectedItems)
		{
			if (iconsToMake.ContainsKey (item.Key.name)) iconsToMake[item.Key.name]++;
			else iconsToMake[item.Key.name] = 1;
			iconSpritesToMake[item.Key.name] = item.Key.icon;
		}

		UIIcon icon;

		foreach (KeyValuePair<string, int> item in iconsToMake)
		{
			icon = getUIIcon ();
			icon.gameObject.SetActive (true);
			icon.SetIcon (this, iconSpritesToMake[item.Key], item.Key, item.Value);

			currentSelectedIcons.Add (icon);
		}

		clearStatusIcons ();

		repeatButton.SetActive (false);

		if (selectedItems.Count == 1 && first != null)
		{
			if (first is UnitSpawner)
			{
				UnitSpawner unitSpawner = (UnitSpawner)first;

				UISelectableStatus status;

				if (unitSpawner.team == game.map.playerTeam)
				{
					bool doneFirst = false;

					foreach (ShipType item in unitSpawner.QueuedItems)
					{
						ShipType itemCopy = item;
						status = getStatusIcon ();
						status.gameObject.SetActive (true);
						currentStatusIcons.Add (status);

						if (doneFirst)
						{
							status.ShowSpawnCount (item.icon, 1, () => { unitSpawner.CancelUnit (itemCopy); });
						}
						else
						{
							status.ShowSpawnPercent (item.icon, unitSpawner.SpawnPercent, () => { unitSpawner.CancelUnit (itemCopy); });
							status.transform.SetAsFirstSibling ();

							doneFirst = true;
						}
					}

					repeatButton.SetActive (true);
					if (unitSpawner.requeue) repeatText.color = repeatButtonOnColour;
					else repeatText.color = repeatButtonOffColour;
				}
			}
			else if (first is Miner)
			{
				Miner miner = (Miner)first;

				UISelectableStatus status;

				if (miner.team == game.map.playerTeam)
				{
					foreach (KeyValuePair<ResourceType, int> item in miner.CarriedResources)
					{
						status = getStatusIcon ();
						status.gameObject.SetActive (true);
						currentStatusIcons.Add (status);

						switch (item.Key)
						{
							case ResourceType.Water:
								status.ShowSpawnCount (waterIcon, item.Value, () => { });
								break;
							case ResourceType.Aluminium:
								status.ShowSpawnCount (aluminiumIcon, item.Value, () => { });
								break;
							case ResourceType.Platinum:
								status.ShowSpawnCount (platinumIcon, item.Value, () => { });
								break;
							case ResourceType.None:
							default:
								status.ShowSpawnCount (null, item.Value, () => { });
								break;
						}

						
					}
				}
			}
		}

		List<Vector2> waypoints = new List<Vector2> ();
		Ship ship;
		ShipStates.FollowComandQueue commandState;
		ShipStates.Patrol patrolState;

		foreach (KeyValuePair<ISelectable, RectTransform> item in selectedItems)
		{
			if (item.Key is UnitSpawner)
			{
				waypoints.Add (((UnitSpawner)item.Key).spawnTarget);
			}
			else if (item.Key is Ship)
			{
				ship = (Ship)item.Key;
				if (ship.stateMachine.IsInState<ShipStates.FollowComandQueue> ())
				{
					commandState = (ShipStates.FollowComandQueue)ship.stateMachine.currentState;
					foreach (ICommand node in commandState.comandQueue.Nodes)
					{
						if (node is MoveCommand) waypoints.Add (((MoveCommand)node).target);
					}
				}
				else if (ship.stateMachine.IsInState<ShipStates.Patrol> ())
				{
					patrolState = (ShipStates.Patrol)ship.stateMachine.currentState;
					foreach (Vector2 node in patrolState.path.Nodes) waypoints.Add (node);
				}
			}
		}

		waypointsDisplay.Show (waypoints);
	}

	public void ToggleRepeatButton ()
	{
		if (selectedItems.Count == 1)
		{
			ISelectable first = getFirstselectedItem ();

			if (first != null && first is UnitSpawner)
			{
				UnitSpawner unitSpawner = (UnitSpawner)first;
				unitSpawner.requeue = !unitSpawner.requeue;
			}
		}
	}

	public void LimitSelection (string name)
	{
		List<ISelectable> selected = new List<ISelectable> (selectedItems.Keys);

		clearSelectedShips ();

		RectTransform rt;

		for (int i = 0; i < selected.Count; i++)
		{
			if (selected[i].name == name)
			{
				rt = getShipHighlight ();
				rt.gameObject.SetActive (true);
				rt.sizeDelta = selected[i].boundsSize * selectedHighlightScale;

				selectedItems.Add (selected[i], rt);
				hasSelectedChanged = true;
			}
		}
	}

	private bool hasSelectionGotType<T> () where T : ISelectable
	{
		foreach (KeyValuePair<ISelectable, RectTransform> item in selectedItems)
		{
			if (item.Key is T) return true;
		}

		return false;
	}

	private void clearSelectionIcons ()
	{
		for (int i = 0; i < currentSelectedIcons.Count; i++)
		{
			currentSelectedIcons[i].gameObject.SetActive (false);
			selectedIconsQueue.Enqueue (currentSelectedIcons[i]);
		}

		currentSelectedIcons.Clear ();
	}

	private void clearStatusIcons ()
	{
		for (int i = 0; i < currentStatusIcons.Count; i++)
		{
			currentStatusIcons[i].gameObject.SetActive (false);
			statusIconsQueue.Enqueue (currentStatusIcons[i]);
		}

		currentStatusIcons.Clear ();
	}

	private UIIcon getUIIcon ()
	{
		if (selectedIconsQueue.Count > 0) return selectedIconsQueue.Dequeue ();
		return Instantiate (uiIconPrefab, selectedIconsHolder);
	}

	private UISelectableStatus getStatusIcon ()
	{
		if (statusIconsQueue.Count > 0) return statusIconsQueue.Dequeue ();
		return Instantiate (statusIconPrefab, statusEffectsHolder);
	}

	private void addSelectedToControlGroup (int selectedIndex)
	{
		// Add the units to the control group
		controlGroups[selectedIndex].Clear ();
		controlGroups[selectedIndex].AddRange (selectedItems.Keys);

		// Make sure to remove the units from any other control gorup
		for (int i = 0; i < controlGroups.Length; i++)
		{
			// Make sure not to remove the items from the new control group
			if (i != selectedIndex) controlGroups[i].RemoveAll (x => controlGroups[selectedIndex].Contains (x));
		}
	}

	private Vector2 clampToScreen (Camera camera, Vector2 pixelPosition)
	{
		return new Vector2 (
			Mathf.Clamp (pixelPosition.x, 0, camera.pixelRect.width),
			Mathf.Clamp (pixelPosition.y, 0, camera.pixelRect.height-1));
	}

	private Rect screenToWroldRect (Camera camera, Rect screenRect)
	{
		Vector2 start = camera.ScreenToWorldPoint (screenRect.position);
		Vector2 end = camera.ScreenToWorldPoint (screenRect.position + screenRect.size);
		return new Rect (start, end - start);
	}

	private RectTransform getShipHighlight ()
	{
		if (shipHighlightQueue.Count > 0) return shipHighlightQueue.Dequeue ();
		else return Instantiate (shipHighlightPrefab, shipHighlightHolder);
	}

	private bool addToExisiting (bool keyDown)
	{
		if (keyDown) return Input.GetKeyDown (KeyCode.LeftShift) || Input.GetKeyDown (KeyCode.RightShift);
		else return Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.RightShift);
	}

	private bool modifyInput (bool keyDown)
	{
		if (keyDown) return Input.GetKeyDown (KeyCode.LeftControl) || Input.GetKeyDown (KeyCode.RightControl);
		else return Input.GetKey (KeyCode.LeftControl) || Input.GetKey (KeyCode.RightControl);
	}

	private void clearSelectedShips ()
	{
		foreach (KeyValuePair<ISelectable, RectTransform> item in selectedItems)
		{
			item.Value.gameObject.SetActive (false);
			shipHighlightQueue.Enqueue (item.Value);
		}

		selectedItems.Clear ();
		hasSelectedChanged = true;
	}

	private void clearHighlightedShips ()
	{
		foreach (KeyValuePair<ISelectable, RectTransform> item in highlightedItems)
		{
			item.Value.gameObject.SetActive (false);
			shipHighlightQueue.Enqueue (item.Value);
		}

		highlightedItems.Clear ();
	}

	private int getControlGroupIndex ()
	{
		if (Input.GetKeyDown (KeyCode.Alpha1)) return 0;
		if (Input.GetKeyDown (KeyCode.Alpha2)) return 1;
		if (Input.GetKeyDown (KeyCode.Alpha3)) return 2;
		if (Input.GetKeyDown (KeyCode.Alpha4)) return 3;
		if (Input.GetKeyDown (KeyCode.Alpha5)) return 4;
		if (Input.GetKeyDown (KeyCode.Alpha6)) return 5;
		if (Input.GetKeyDown (KeyCode.Alpha7)) return 6;
		if (Input.GetKeyDown (KeyCode.Alpha8)) return 7;
		if (Input.GetKeyDown (KeyCode.Alpha9)) return 8;
		return -1;
	}

	private int getControlGroupIndex (KeyCode keycode)
	{
		switch (keycode)
		{
			case KeyCode.Alpha1: return 0;
			case KeyCode.Alpha2: return 1;
			case KeyCode.Alpha3: return 2;
			case KeyCode.Alpha4: return 3;
			case KeyCode.Alpha5: return 4;
			case KeyCode.Alpha6: return 5;
			case KeyCode.Alpha7: return 6;
			case KeyCode.Alpha8: return 7;
			case KeyCode.Alpha9: return 8;
			default: return -1;
		}
	}
}
