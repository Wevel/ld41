﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
	[System.Serializable]
	public struct SpawnNamesList
	{
		public List<SpawnNameCount> names;
	}

	[System.Serializable]
	public struct SpawnNameCount
	{
		public string name;
		public int count;
	}

	public CameraController cameraController;

	public Material shipMaterial;
	public Mesh workerMesh;
	public Mesh commandCentreMesh;
	public float mapSize = 50;

	public Color playerUnitColour = Color.white;
	public Color playerBuildingColour = Color.white;
	public Color enemyUnitColour = Color.white;
	public Color enemyBuildingColour = Color.white;

	public Texture2D playerShipTexture;
	public Texture2D enemyShipTexture;

	public Map map { get; private set; }

	public GameObject mapPrefab;

	public Transform commandCenterLocation;

	public bool paused = false;
	public bool gameOver { get; private set; }

	public float waveDelay = 45;
	public float spawnDelay = 0.5f;

	public int currentWaveCount { get; private set; }

	public List<Transform> enemyPathNodes;

	public float nextWaveTimer { get; private set; }

	public List<SpawnNamesList> initialWaves = new List<SpawnNamesList> ();

	private bool spawningWave = false;

	// Update is called once per frame
	void Update ()
	{
		if (!paused && map != null && !gameOver)
		{
			if (!spawningWave)
			{
				if (nextWaveTimer > 0) nextWaveTimer -= Time.deltaTime;
				else SpawnNextWave ();
			}

			map.Update (Time.deltaTime);

			if (map.playerCommandCenter != null && !map.playerCommandCenter.isAlive)
			{
				// The players command center is dead, so its gg
				gameOver = true;
			}
		}
	}

	public void StartGame ()
	{
		if (map != null)
		{
			// Clear the old map
			map.Clear ();
			map = null;
		}

		Path enemyPath = new Path ();
		for (int i = 0; i < enemyPathNodes.Count; i++) enemyPath.AddWaypoint (enemyPathNodes[i].localPosition);

		map = new Map (mapSize);

		map.Init (
			new Team (map, playerUnitColour, playerBuildingColour, playerShipTexture, 10),
			new Team (map, enemyUnitColour, enemyBuildingColour, enemyShipTexture, 10),
			commandCenterLocation.localPosition,
			enemyPath);

		cameraController.SetLimitRectangle (new Rect (0, 0, mapSize, mapSize));
		cameraController.SetLocation (commandCenterLocation.localPosition);

		AsteroidSpawn[] asteroids = mapPrefab.GetComponentsInChildren<AsteroidSpawn> (true);
		for (int i = 0; i < asteroids.Length; i++) asteroids[i].Spawn (map);

		paused = false;
		gameOver = false;

		currentWaveCount = 0;
		nextWaveTimer = 2f * waveDelay;
	}

	public void EndGame ()
	{
		map.Clear ();
		map = null;
	}

	public void SpawnNextWave ()
	{
		nextWaveTimer = waveDelay;
		StartCoroutine (spawnNextWave (currentWaveCount));
		currentWaveCount++;
	}

	private IEnumerator spawnNextWave (int wave)
	{
		while (spawningWave) yield return new WaitForEndOfFrame ();

		spawningWave = true;

		List<string> toSpawn = getSpawnNames (currentWaveCount);

		for (int i = 0; i < toSpawn.Count; i++)
		{
			map.SpawnEnemy (toSpawn[i]);
			yield return new WaitForSeconds (spawnDelay);
		}

		spawningWave = false;
	}

	private List<string> getSpawnNames (int wave)
	{
		List<string> names = new List<string> ();

		if (wave < initialWaves.Count)
		{
			SpawnNamesList list = initialWaves[wave];
			for (int i = 0; i < list.names.Count; i++)
			{
				for (int a = 0; a < list.names[i].count; a++) names.Add (list.names[i].name);
			}
		}
		else
		{
			SpawnNamesList list = initialWaves[initialWaves.Count - 1];
			int bonus;
			for (int i = 0; i < list.names.Count; i++)
			{
				bonus = wave - initialWaves.Count;
				if (list.names[i].name == "Scout") bonus *= 3;
				else if (list.names[i].name == "Frigate") bonus *= 2;
				for (int a = 0; a < list.names[i].count + bonus; a++) names.Add (list.names[i].name);
			}
		}

		return names;
	}

	private void OnDrawGizmosSelected ()
	{
		if (map != null)
		{
			foreach (Agent item in map.agentManager.Agents)
			{
				Gizmos.color = Color.red;
				Gizmos.DrawRay (item.position, item.velocity);
			}

			foreach (ICollidable item in map.Objects) item.OnDrawGizmos ();
		}
	}
}
