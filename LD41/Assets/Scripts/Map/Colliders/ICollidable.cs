﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICollidable
{
	float CollisionTestRange
	{
		get;
	}

	void Destroy ();
	void Update (float deltaTime);
	void OnCollisionStay (ICollidable other);
	bool DoesCollide (Agent agent, float deltaTime, ref Vector2 positionDelta);

	void OnDrawGizmos ();
}
