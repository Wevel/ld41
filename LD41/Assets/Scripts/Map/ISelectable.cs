﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISelectable
{
	Vector2 position { get; }
	Vector2 boundsSize { get; }
	string name { get; }
	Sprite icon { get; }
	int currentHealth { get; }
	int maxHealth { get; }
	bool isAlive { get; }

	void TryQueueBuild (string name, Vector2 at);

	void Destroy ();
}
