﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : ISelectable, ICollidable
{
	public readonly Map map;

	public Vector2 position { get; private set; }
	public Vector2 collisionOffset { get; private set; }
	public Vector2 boundsSize { get; private set; }
	public string name { get; private set; }
	public Sprite icon { get; private set; }
	public int currentHealth { get; private set; }
	public int maxHealth { get; private set; }
	public float collisionRadius { get; private set; }
	
	public ResourceType resourceType { get; private set; }

	public float CollisionTestRange { get; private set; }

	public AsteroidDisplay display { get; private set; }

	public bool isAlive
	{
		get
		{
			return currentHealth > 0;
		}
	}

	public Asteroid (Map map, Vector2 position, Sprite icon, string name, float collisionRadius, Vector2 collisionOffset)
	{
		this.map = map;
		this.position = position;
		this.icon = icon;
		this.name = name;
		this.maxHealth = 99999;
		this.currentHealth = 99999;
		this.collisionRadius = collisionRadius;
		this.collisionOffset = collisionOffset;

		CollisionTestRange = collisionRadius * 2f;
		resourceType = ResourceType.None;

		boundsSize = Vector2.one * collisionRadius;

		map.AddCollidable (this);
		map.AddAsteroid (this);
	}

	public void AddResources (ResourceType type, int count)
	{
		resourceType = type;
		currentHealth = count;
		maxHealth = count;
	}

	public void Mine (int amount)
	{
		if (!CanMine ()) return;

		currentHealth -= amount;
		if (currentHealth <= 0) currentHealth = 0;
	}

	public bool CanMine ()
	{
		return currentHealth >= 0 && resourceType != ResourceType.None;
	}

	public void SetDisplay (AsteroidDisplay display)
	{
		if (this.display != null) this.display.SetAgent (null);
		if (display != null) display.SetAgent (this);
		this.display = display;
	}

	public void Destroy ()
	{
		map.RemoveCollidable (this);
		map.RemoveAsteroid (this);
		display.SetAgent (null);
	}

	public void TryQueueBuild (string name, Vector2 at) { }
	public void Update (float deltaTime) { }
	public void OnCollisionStay (ICollidable other) { }

	public bool DoesCollide (Agent agent, float deltaTime, ref Vector2 positionDelta)
	{
		Vector2 agentPositionDelta = this.position - agent.position - collisionOffset;
		if (Vector2.Dot (positionDelta, agentPositionDelta) > 0)
		{
			float seperation = agentPositionDelta.magnitude;
			float dist = seperation - agent.collisionRadius - this.collisionRadius;

			if (dist > 0)
			{
				float projection = Vector2.Dot (positionDelta, agentPositionDelta) / seperation;
				if (dist < projection)
				{
					positionDelta *= Mathf.Clamp01 (dist / projection);
					return true;
				}
			}
			else
			{
				// We already overlap
				positionDelta += agentPositionDelta * dist;
				return true;
			}
		}

		return false;
	}

	public void OnDrawGizmos ()
	{
		Gizmos.color = Color.cyan;
		Gizmos.DrawWireSphere (position + collisionOffset, collisionRadius);
	}
}
