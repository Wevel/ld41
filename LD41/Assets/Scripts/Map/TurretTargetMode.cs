﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TurretTargetMode
{
	Strongest,
	Weakest,
	ClosestToEnd,
	FurthestFromEnd
}