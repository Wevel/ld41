﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AsteroidSpawn : MonoBehaviour
{
	public enum AsteroidMesh
	{
		A, B, C, D
	}

	public ResourceType resources;
	public AsteroidMesh mesh;

	public int resourceCount = 0;

	public Mesh asteroidAMesh;
	public Mesh asteroidBMesh;
	public Mesh asteroidCMesh;
	public Mesh asteroidDMesh;

	public Sprite asteroidAIcon;
	public Sprite asteroidBIcon;
	public Sprite asteroidCIcon;
	public Sprite asteroidDIcon;

	public float asteroidASize;
	public float asteroidBSize;
	public float asteroidCSize;
	public float asteroidDSize;

	public Vector2 asteroidAOffset;
	public Vector2 asteroidBOffset;
	public Vector2 asteroidCOffset;
	public Vector2 asteroidDOffset;

	public Mesh waterMarkerMesh;
	public Mesh aluminiumMarkerMesh;
	public Mesh platinumMarkerMesh;

	public GameObject waterMarker;
	public GameObject aluminiumMarker;
	public GameObject platinumMarker;

	private void Update ()
	{
		if (waterMarker != null) waterMarker.SetActive (resources == ResourceType.Water);
		if (aluminiumMarker != null) aluminiumMarker.SetActive (resources == ResourceType.Aluminium);
		if (platinumMarker != null) platinumMarker.SetActive (resources == ResourceType.Platinum);

		this.GetComponent<MeshFilter> ().sharedMesh = getMainMesh ();
	}

	public void Spawn (Map map)
	{
		Mesh markerMesh = getMergerMesh ();

		Mesh finalMesh;

		if (markerMesh != null)
		{
			CombineInstance[] toCombine = new CombineInstance[2];
			toCombine[0] = new CombineInstance () { mesh = getMainMesh (), subMeshIndex = 0, transform = Matrix4x4.identity };
			toCombine[1] = new CombineInstance () { mesh = markerMesh, subMeshIndex = 0, transform = Matrix4x4.identity };

			finalMesh = new Mesh ();
			finalMesh.CombineMeshes (toCombine, true);
		}
		else
		{
			finalMesh = getMainMesh ();
		}

		Asteroid asteroid = new Asteroid (map, transform.localPosition, getMainIcon (), getName (), getSize (), getCollisionOffset ());
		AsteroidDisplay.AddDisplay (asteroid, finalMesh, GetComponent<MeshRenderer> ().material);

		if (resources != ResourceType.None) asteroid.AddResources (resources, resourceCount);
	}

	private float getSize ()
	{
		switch (mesh)
		{
			case AsteroidMesh.A: return asteroidASize;
			case AsteroidMesh.B: return asteroidBSize;
			case AsteroidMesh.C: return asteroidCSize;
			case AsteroidMesh.D: return asteroidDSize;
			default: return 1;
		}
	}

	private Vector2 getCollisionOffset ()
	{
		switch (mesh)
		{
			case AsteroidMesh.A: return asteroidAOffset;
			case AsteroidMesh.B: return asteroidBOffset;
			case AsteroidMesh.C: return asteroidCOffset;
			case AsteroidMesh.D: return asteroidDOffset;
			default: return Vector2.zero;
		}
	}

	private Mesh getMainMesh ()
	{
		switch (mesh)
		{
			case AsteroidMesh.A: return asteroidAMesh;
			case AsteroidMesh.B: return asteroidBMesh;
			case AsteroidMesh.C: return asteroidCMesh;
			case AsteroidMesh.D: return asteroidDMesh;
			default: return null;
		}
	}

	private Sprite getMainIcon ()
	{
		switch (mesh)
		{
			case AsteroidMesh.A: return asteroidAIcon;
			case AsteroidMesh.B: return asteroidBIcon;
			case AsteroidMesh.C: return asteroidCIcon;
			case AsteroidMesh.D: return asteroidDIcon;
			default: return null;
		}
	}

	private Mesh getMergerMesh ()
	{
		switch (resources)
		{
			case ResourceType.Water: return waterMarkerMesh;
			case ResourceType.Aluminium: return aluminiumMarkerMesh;
			case ResourceType.Platinum: return platinumMarkerMesh;
			case ResourceType.None:
			default: return null;
		}
	}

	private string getName ()
	{
		switch (resources)
		{
			case ResourceType.Water: return "Water Asteroid";
			case ResourceType.Aluminium: return "Aluminium Asteroid";
			case ResourceType.Platinum: return "Platinum Asteroid";
			case ResourceType.None:
			default: return "Asteroid";
		}
	}
}
