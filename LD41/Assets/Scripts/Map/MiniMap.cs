﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour
{
	public Game game;
	public Camera miniMapCamera;

	public Material material;
	public Color asteroidColour = Color.white;
	public Color waterAsteroidColour = Color.white;
	public Color aluminiumAsteroidColour = Color.white;
	public Color platinumAsteroidColour = Color.white;

	private void OnRenderObject ()
	{
		if (Camera.current == miniMapCamera)
		{
			if (game != null && game.map != null)
			{
				miniMapCamera.transform.position = (Vector3)(Vector2.one * game.map.size / 2f) + new Vector3 (0, 0, -10);
				miniMapCamera.orthographicSize = game.map.size / 2f;

				material.SetPass (0);

				GL.Begin (GL.TRIANGLES);

				foreach (Asteroid item in game.map.Asteroids) drawMesh (item.position, getAsteroidColour (item.resourceType), 1f);

				GL.End ();
				GL.Flush ();

				GL.Begin (GL.TRIANGLES);

				foreach (Building item in game.map.Buildings) drawMesh (item.position, item.team.buildingColour, 1f);

				GL.End ();
				GL.Flush ();
				GL.Begin (GL.TRIANGLES);

				foreach (Agent item in game.map.agentManager.Agents) drawMesh (item.position, item.team.unitColour, 0.5f);

				GL.End ();
				GL.End ();
				GL.Flush ();
			}
		}
	}

	private Color getAsteroidColour (ResourceType type)
	{
		switch (type)
		{
			case ResourceType.Water: return waterAsteroidColour;
			case ResourceType.Aluminium: return aluminiumAsteroidColour;
			case ResourceType.Platinum: return platinumAsteroidColour;
			case ResourceType.None:
			default: return asteroidColour;
		}
	}

	private void drawMesh (Vector2 position, Color colour, float halfSize)
	{
		// 0
		GL.Color (colour);
		GL.TexCoord2 (0, 0);
		GL.Vertex3 (position.x - halfSize, position.y - halfSize, 0);

		// 1
		GL.Color (colour);
		GL.TexCoord2 (0, 1);
		GL.Vertex3 (position.x - halfSize, position.y + halfSize, 0);

		// 2
		GL.Color (colour);
		GL.TexCoord2 (1, 1);
		GL.Vertex3 (position.x + halfSize, position.y + halfSize, 0);

		// 0
		GL.Color (colour);
		GL.TexCoord2 (0, 0);
		GL.Vertex3 (position.x - halfSize, position.y - halfSize, 0);

		// 2
		GL.Color (colour);
		GL.TexCoord2 (1, 1);
		GL.Vertex3 (position.x + halfSize, position.y + halfSize, 0);

		// 3
		GL.Color (colour);
		GL.TexCoord2 (1, 0);
		GL.Vertex3 (position.x + halfSize, position.y - halfSize, 0);
	}
}
