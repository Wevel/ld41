﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Building : ICollidable, ISelectable, ITargetable
{
	public readonly Map map;
	public readonly Team team;
	public readonly BuildingType type;
	public Vector2 position { get; private set; }

	public bool hasBeenBuiltFully { get; private set; }

	public Vector2 velocity
	{
		get
		{
			return Vector2.zero;
		}
	}

	public int maxHealth
	{
		get
		{
			return type.maxHealth;
		}
	}

	private float _currentHealth = 1;
	public int currentHealth
	{
		get
		{
			return Mathf.Clamp (Mathf.FloorToInt (_currentHealth), 0, type.maxHealth);
		}
		private set
		{
			_currentHealth = Mathf.Clamp (value, 0, type.maxHealth);
		}
	}

	public Vector2 boundsSize
	{
		get
		{
			return type.size;
		}
	}

	public string name
	{
		get
		{
			return type.name;
		}
	}

	private Rect _collisionRectangle;
	public Rect collisionRectangle
	{
		get
		{
			return _collisionRectangle;
		}
		private set
		{
			_collisionRectangle = value;

			float xMaxDist = Mathf.Max (Mathf.Abs (_collisionRectangle.xMin), Mathf.Abs (_collisionRectangle.xMax));
			float yMaxDist = Mathf.Max (Mathf.Abs (_collisionRectangle.yMin), Mathf.Abs (_collisionRectangle.yMax));
			collisionTestRange = xMaxDist + yMaxDist;
		}
	}

	public float collisionTestRange { get; private set; }

	public float CollisionTestRange
	{
		get
		{
			return collisionTestRange;
		}
	}

	public bool Built
	{
		get
		{
			return currentHealth >= type.maxHealth;
		}
	}

	public bool CanWork
	{
		get
		{
			return hasBeenBuiltFully;
		}
	}

	public Sprite icon
	{
		get
		{
			return type.icon;
		}
	}

	public Vector2 targetPosition
	{
		get
		{
			return position;
		}
	}

	public bool isAlive
	{
		get
		{
			return currentHealth > 0;
		}
	}

	public BuildingDisplay display { get; private set; }

	public Building (Map map, Team team, Vector2 position, BuildingType type)
	{
		this.map = map;
		this.team = team;
		this.type = type;
		this.position = position;

		display = null;
		collisionRectangle = new Rect (position - (type.size / 2f), type.size);

		map.AddCollidable (this);
		map.AddBuilding (this);

		// Put one point in so that it sort of has health
		currentHealth = 1;

		team.HasBuildBuilding (type);
	}

	public void Destroy ()
	{
		map.RemoveCollidable (this);
		map.RemoveBuilding (this);
		display.SetBuilding (null);
		OnDestroy ();

		team.RecalculateFleetLimit ();
	}

	public virtual void Update (float deltaTime) { }
	protected virtual void OnDestroy () { }
	public virtual void TryQueueBuild (string name, Vector2 at) { }

	public Vector2 GetInRangePosition (Vector2 otherPosition, float range)
	{
		float distance = Vector2.Distance (position, otherPosition);
		if (distance < range) return otherPosition;

		float moveDistance = distance - range - 1;
		return otherPosition + Vector2.ClampMagnitude (position - otherPosition, moveDistance);
	}

	public void InstaBuild ()
	{
		currentHealth = type.maxHealth;
		hasBeenBuiltFully = true;
		team.RecalculateFleetLimit ();
	}

	public void DoBuildProgress (float amount)
	{
		if (currentHealth >= type.maxHealth) return;
		_currentHealth += amount;

		if (Built)
		{
			hasBeenBuiltFully = true;
			team.RecalculateFleetLimit ();
		}
	}

	public void OnCollisionStay (ICollidable other)
	{
		if (other is Projectile)
		{
			Projectile projectile = (Projectile)other;

			if (projectile.CanDamage && projectile.team != team)
			{
				List<Building> inRange = map.GetBuildingsInRange (projectile.position, projectile.splashDamageRange, (ship) =>
				{
					return projectile.team != team;
				});

				// Make sure that this building is encluded in the damaged building
				if (!inRange.Contains (this)) inRange.Add (this);

				for (int i = 0; i < inRange.Count; i++) inRange[i].currentHealth -= projectile.damage;

				projectile.Destroy ();
				if (!isAlive) Destroy ();
			}
		}
	}

	public void DoDamage (int amount)
	{
		currentHealth -= amount;
		if (!isAlive) Destroy ();
	}

	public void SetDisplay (BuildingDisplay display)
	{
		if (this.display != null) this.display.SetBuilding (null);
		if (display != null) display.SetBuilding (this);
		this.display = display;
	}

	public bool DoesCollide (Agent agent, float deltaTime, ref Vector2 positionDelta)
	{
		const float additionalPush = 1.2f;

		bool didCollide = false;
		float dist;

		Rect r = collisionRectangle;

		// Left edge
		if ((agent.position.y < r.yMax || agent.position.x < r.xMin)
			&& (agent.position.y > r.yMin || agent.position.x < r.xMin))
		{
			dist = r.xMin - agent.position.x - agent.collisionRadius;
			if (agent.position.y > r.yMax) dist += agent.position.y - r.yMax;
			else if (agent.position.y < r.yMin) dist += r.yMin - agent.position.y;
			if (dist < 0 && agent.position.x < position.x)
			{
				positionDelta += new Vector2 (dist * additionalPush, 0);
				didCollide = true;
			}
		}

		// Right edge
		if ((agent.position.y < r.yMax || agent.position.x > r.xMax)
			&& (agent.position.y > r.yMin || agent.position.x > r.xMax))
		{
			dist = agent.position.x - agent.collisionRadius - r.xMax;
			if (agent.position.y > r.yMax) dist += agent.position.y - r.yMax;
			else if (agent.position.y < r.yMin) dist += r.yMin - agent.position.y;
			if (dist < 0 && agent.position.x > position.x)
			{
				positionDelta += new Vector2 (-dist * additionalPush, 0);
				didCollide = true;
			}
		}

		if (agent.position.x > r.xMin
			&& agent.position.x < r.xMax)
		{
			// Bottom edge
			dist = r.yMin - agent.position.y - agent.collisionRadius;
			if (dist < 0 && agent.position.y < position.y)
			{
				positionDelta += new Vector2 (0, dist * additionalPush);
				didCollide = true;
			}

			// Top edge
			dist = agent.position.y - agent.collisionRadius - r.yMax;
			if (dist < 0 && agent.position.y > position.y)
			{
				positionDelta += new Vector2 (0, -dist * additionalPush);
				didCollide = true;
			}
		}

		return didCollide;
	}

	public void OnDrawGizmos ()
	{
		Rect r = collisionRectangle;

		Gizmos.color = Color.yellow;
		Gizmos.DrawLine (new Vector3 (r.xMin, r.yMin), new Vector3 (r.xMin, r.yMax));
		Gizmos.DrawLine (new Vector3 (r.xMin, r.yMax), new Vector3 (r.xMax, r.yMax));
		Gizmos.DrawLine (new Vector3 (r.xMax, r.yMax), new Vector3 (r.xMax, r.yMin));
		Gizmos.DrawLine (new Vector3 (r.xMax, r.yMin), new Vector3 (r.xMin, r.yMin));
	}

	public abstract float GetStrengthFactor ();
}
