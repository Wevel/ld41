﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDisplay : MonoBehaviour
{
	private static Transform holder;
	private static Queue<BuildingDisplay> unusedDisplayes = new Queue<BuildingDisplay> ();

	static BuildingDisplay ()
	{
		holder = new GameObject ("BuildingDisplayHolder").transform;
	}

	public static void AddDisplay (Building building, Mesh mesh, Material material)
	{
		BuildingDisplay display;

		if (unusedDisplayes.Count > 0)
		{
			display = unusedDisplayes.Dequeue ();
			display.gameObject.SetActive (true);
		}
		else
		{
			GameObject go = new GameObject ("BuildingDisplay");
			go.transform.SetParent (holder);
			go.AddComponent<MeshFilter> ();
			go.AddComponent<MeshRenderer> ();
			display = go.AddComponent<BuildingDisplay> ();
		}

		display.mesh = mesh;
		display.material = material;

		building.SetDisplay (display);
	}

	private static void stopUsingDisplay (BuildingDisplay display)
	{
		display.gameObject.SetActive (false);
		unusedDisplayes.Enqueue (display);
	}

	public Mesh mesh;

	private Material _material;
	public Material material
	{
		get
		{
			return _material;
		}
		set
		{
			if (_material != null) Destroy (_material);
			_material = new Material (value);
		}
	}

	private Building building = null;

	private MeshFilter mf;
	private MeshRenderer mr;

	private void Awake ()
	{
		mf = this.GetComponent<MeshFilter> ();
		mr = this.GetComponent<MeshRenderer> ();
	}

	private void Update ()
	{
		if (building != null)
		{
			updatePosition ();
			updateDisplay ();
		}
	}

	private void OnDestroy ()
	{
		if (material != null) Destroy (material);
	}

	public void SetBuilding (Building building)
	{
		this.building = building;

		if (building != null)
		{
			updatePosition ();
			updateDisplay ();
		}
		else
		{
			stopUsingDisplay (this);
		}
	}

	private void updatePosition ()
	{
		transform.position = building.position;
		transform.rotation = Quaternion.identity;
	}

	private void updateDisplay ()
	{
		material.SetFloat ("_BuiltPercent", building.currentHealth / (float)building.type.maxHealth);
		if (mesh.subMeshCount > 1) material.SetVector ("_Offset", new Vector4 (building.position.x, building.position.y, 0, 0));
		else material.SetVector ("_Offset", new Vector4 (0, 0, 0, 0));

		Material[] materials = new Material[mesh.subMeshCount];
		for (int i = 0; i < mesh.subMeshCount; i++) materials[i] = material;
		mr.sharedMaterials = materials;

		if (building == null) mf.sharedMesh = null;
		else mf.sharedMesh = mesh;
	}
}
