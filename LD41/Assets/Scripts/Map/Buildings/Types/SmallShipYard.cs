﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallShipYard : UnitSpawner
{
	public SmallShipYard (Map map, Team team, Vector2 position, BuildingType type) : base (map, team, position, type) { }
}