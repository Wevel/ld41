﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : Building
{
	public Gun gun;

	public Turret (Map map, Team team, Vector2 position, BuildingType type, Gun gun) : base (map, team, position, type)
	{
		this.gun = gun;
	}

	public override void Update (float deltaTime)
	{
		if (hasBeenBuiltFully) gun.Update (position, Vector2.zero, deltaTime);
	}

	public void TryTarget (Vector2 targetPosition)
	{
		gun.TryTarget (targetPosition);
	}

	public override float GetStrengthFactor ()
	{
		if (gun is LaserGun) return 10;
		else if (gun is MissileGun) return 7.5f;
		else if (gun is KineticGun) return 7.5f;

		return 0;
	}
}