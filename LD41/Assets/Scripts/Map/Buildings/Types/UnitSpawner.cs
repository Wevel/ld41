﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSpawner : Building
{
	public Vector2 spawnTarget;
	public float spawnTimeModifier = 1;
	public bool requeue = false;

	private Queue<ShipType> spawnQueue = new Queue<ShipType> ();

	private float spawnTimer = 0;

	public IEnumerable<ShipType> QueuedItems
	{
		get
		{
			foreach (ShipType item in spawnQueue) yield return item;
		}
	}

	public int SpawnPercent
	{
		get
		{
			if (spawnQueue.Count == 0) return 0;
			ShipType first = spawnQueue.Peek ();
			return Mathf.FloorToInt (100f * Mathf.Clamp01 (spawnTimer / first.spawnTime));
		}
	}

	public UnitSpawner (Map map, Team team, Vector2 position, BuildingType type) : base (map, team, position, type)
	{
		spawnTarget = position + (type.size / 2f) + new Vector2 (0.25f, 0.25f);
	}

	public override void Update (float deltaTime)
	{
		if (CanWork)
		{
			if (spawnQueue.Count > 0)
			{
				spawnTimer += deltaTime;

				ShipType type;
				while (spawnQueue.Count > 0 && spawnTimer > (type = spawnQueue.Peek ()).spawnTime)
				{
					if (!team.CanSpawnShip (type))
					{
						spawnTimer -= deltaTime;
						break;
					}

					spawnTimer -= type.spawnTime;
					spawnQueue.Dequeue ();
					spawnAgentNow (type);

					// Make sure to requeue the type, if we have been told to
					if (requeue) spawnQueue.Enqueue (type);
				}
			}
			else
			{
				spawnTimer = 0;
			}
		}
	}

	public override void TryQueueBuild (string name, Vector2 at)
	{
		ShipType type = Defines.GetShipType (name);
		if (type != null && Defines.GetBuildingType (name) == null)
		{
			if (team.CanQueueShip (type))
			{
				team.HasQueuedUpShipBuild (type);
				spawnQueue.Enqueue (type);
			}
		}
		else
		{
			Debug.LogWarning ("No ship with name: " + name);
		}
	}

	public void CancelUnit (ShipType type)
	{
		List<ShipType> types = new List<ShipType> (spawnQueue);
		spawnQueue.Clear ();

		bool removed = false;

		for (int i = 0; i < types.Count; i++)
		{
			if (!removed && types[i] == type) removed = true;
			else spawnQueue.Enqueue (types[i]);
		}

		if (removed) team.HasCanceledShipBuild (type);
	}

	public void SpawnUnit (ShipType type)
	{
		spawnQueue.Enqueue (type);
	}

	private void spawnAgentNow (ShipType type)
	{
		Vector2 spawnPosition = spawnTarget;
		spawnPosition.x = Mathf.Clamp (spawnPosition.x, collisionRectangle.xMin - type.collisionRange, collisionRectangle.xMax + type.collisionRange);
		spawnPosition.y = Mathf.Clamp (spawnPosition.y, collisionRectangle.yMin - type.collisionRange, collisionRectangle.yMax + type.collisionRange);
		Vector2 spawnPositionDelta = spawnPosition - position;

		Ship ship = type.SpawnShipNow (map, team, spawnPosition, Mathf.Atan2 (spawnPositionDelta.y, spawnPositionDelta.x) * Mathf.Rad2Deg);
		ship.stateMachine.SetState (new ShipStates.GotoLocation (spawnTarget, 1f));
	}

	public override float GetStrengthFactor ()
	{
		return 0.5f;
	}
}