﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hangar : Building
{
	public Hangar (Map map, Team team, Vector2 position, BuildingType type) : base (map, team, position, type) { }

	public override float GetStrengthFactor ()
	{
		return 0.25f;
	}
}