﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LargeShipYard : UnitSpawner
{
	public LargeShipYard (Map map, Team team, Vector2 position, BuildingType type) : base (map, team, position, type) { }
}