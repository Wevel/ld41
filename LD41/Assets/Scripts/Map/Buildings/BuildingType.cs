﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingType
{
	public delegate Building SpawnBuilding (Map map, Team team, Vector2 position, BuildingType type);

	public readonly string name;
	public readonly int maxHealth;
	public readonly int fleetLimitBonus;
	public readonly Vector2 size;
	public readonly Sprite icon;
	public SpawnBuilding spawnBuilding;
	public Mesh mesh;
	public Material material;
	public readonly int platinumCost;
	public readonly int aluminiumCost;
	public readonly int waterCost;

	public Vector2 collisionOffset = Vector2.zero;

	public BuildingType (string name, int maxHealth, Vector2 size, int fleetLimitBonus, Sprite icon, Mesh mesh, Material material, int platinumCost, int aluminiumCost, int waterCost, SpawnBuilding spawnShip)
	{
		this.name = name;
		this.maxHealth = maxHealth;
		this.size = size;
		this.fleetLimitBonus = fleetLimitBonus;
		this.icon = icon;
		this.spawnBuilding = spawnShip;
		this.mesh = mesh;
		this.material = material;
		this.platinumCost = platinumCost;
		this.aluminiumCost = aluminiumCost;
		this.waterCost = waterCost;
	}
}
