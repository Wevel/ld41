﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map
{
	public readonly float size;

	public readonly AgentManager agentManager;

	public Team playerTeam { get; private set; }
	public Team enemyTeam { get; private set; }

	public CommandCenter playerCommandCenter { get; private set; }

	private List<ICollidable> collidableObjects = new List<ICollidable> ();
	private List<Building> buildings = new List<Building> ();
	private List<Asteroid> asteroids = new List<Asteroid> ();

	private Path enemyPath;

	public IEnumerable<ICollidable> Objects
	{
		get
		{
			for (int i = 0; i < collidableObjects.Count; i++) yield return collidableObjects[i];
		}
	}

	public IEnumerable<Building> Buildings
	{
		get
		{
			for (int i = 0; i < buildings.Count; i++) yield return buildings[i];
		}
	}

	public IEnumerable<Asteroid> Asteroids
	{
		get
		{
			for (int i = 0; i < asteroids.Count; i++) yield return asteroids[i];
		}
	}

	public Map (float size)
	{
		this.size = size;

		agentManager = new AgentManager ();
	}

	public void Init (Team playerTeam, Team enemyTeam, Vector2 commandCenterLocation, Path enemyPath)
	{
		if (this.playerTeam != null || this.enemyTeam != null) Debug.LogWarning ("Already has teams assigned");

		// Teams
		this.playerTeam = playerTeam;
		this.enemyTeam = enemyTeam;

		this.enemyPath = enemyPath;

		// Player command center
		BuildingType commandCentreType = Defines.GetBuildingType ("Command Center");

		playerTeam.AddResources (ResourceType.Platinum, commandCentreType.platinumCost );
		playerTeam.AddResources (ResourceType.Aluminium, commandCentreType.aluminiumCost);
		playerTeam.AddResources (ResourceType.Water, commandCentreType.waterCost);

		playerCommandCenter = (CommandCenter)commandCentreType.spawnBuilding (this, playerTeam, commandCenterLocation, commandCentreType);
		playerCommandCenter.InstaBuild ();

		// Spawn initial ships
		ShipType workerType = Defines.GetShipType ("Worker");
		ShipType minerType = Defines.GetShipType ("Miner");

		// Give the player the resources for these ships, so it doesn't cut into their initial ones
		playerTeam.AddResources (ResourceType.Platinum, workerType.platinumCost + (3 * minerType.platinumCost));
		playerTeam.AddResources (ResourceType.Aluminium, workerType.aluminiumCost + (3 * minerType.aluminiumCost));
		playerTeam.AddResources (ResourceType.Water, workerType.waterCost + (3 * minerType.waterCost));

		Vector2 spawnDelta;
		Ship ship = workerType.SpawnShipNow (
			this,
			playerTeam, 
			commandCenterLocation + (spawnDelta = new Vector2 (Random.Range (2f, 3f), 
			Random.Range (2f, 3f))), Mathf.Atan2 (spawnDelta.y, spawnDelta.x) * Mathf.Rad2Deg);
		ship.stateMachine.SetState (new ShipStates.Idle (ship.position));

		ship = minerType.SpawnShipNow (
			this,
			playerTeam,
			commandCenterLocation + (spawnDelta = new Vector2 (Random.Range (2f, 3f), 
			Random.Range (2f, 3f))), Mathf.Atan2 (spawnDelta.y, spawnDelta.x) * Mathf.Rad2Deg);
		ship.stateMachine.SetState (new ShipStates.Idle (ship.position));

		ship = minerType.SpawnShipNow (
			this,
			playerTeam,
			commandCenterLocation + (spawnDelta = new Vector2 (Random.Range (2f, 3f),
			Random.Range (2f, 3f))), Mathf.Atan2 (spawnDelta.y, spawnDelta.x) * Mathf.Rad2Deg);
		ship.stateMachine.SetState (new ShipStates.Idle (ship.position));

		ship = minerType.SpawnShipNow (
			this, 
			playerTeam,
			commandCenterLocation + (spawnDelta = new Vector2 (Random.Range (2f, 3f),
			Random.Range (2f, 3f))), Mathf.Atan2 (spawnDelta.y, spawnDelta.x) * Mathf.Rad2Deg);
		ship.stateMachine.SetState (new ShipStates.Idle (ship.position));

		// Give player initial resources

		playerTeam.AddResources (ResourceType.Platinum, 200);
		playerTeam.AddResources (ResourceType.Aluminium, 200);
		playerTeam.AddResources (ResourceType.Water, 200);

		// Just give the enemy infinite resources
		enemyTeam.AddResources (ResourceType.Platinum, 999999999);
		enemyTeam.AddResources (ResourceType.Aluminium, 999999999);
		enemyTeam.AddResources (ResourceType.Water, 999999999);
	}

	public void SpawnEnemy (string name)
	{
		Ship ship = null;

		ShipType type = Defines.GetShipType (name);
		if (type != null) ship = type.SpawnShipNow (this, this.enemyTeam, enemyPath.Start + new Vector2(Random.Range (-1f, 1f), Random.Range (-1f, 1f)), 180);
		else Debug.LogError ("No Ship with name: " + name);

		if (ship != null)
		{
			ship.stateMachine.SetState (new ShipStates.Enemy (enemyPath));
		}
	}

	public void Clear ()
	{
		for (int i = buildings.Count - 1; i >= 0; i--) buildings[i].Destroy ();
		for (int i = asteroids.Count - 1; i >= 0; i--) asteroids[i].Destroy ();
		agentManager.Clear ();

		// If there are any remaining objects, remove them, this will be things like bullets ect.
		for (int i = collidableObjects.Count - 1; i >= 0; i--) collidableObjects[i].Destroy ();
	}

	public void Update (float deltaTime)
	{
		for (int i = 0; i < collidableObjects.Count; i++) collidableObjects[i].Update (deltaTime);
	}

	public void AddCollidable (ICollidable collidable)
	{
		if (!collidableObjects.Contains (collidable)) collidableObjects.Add (collidable);
	}

	public void RemoveCollidable (ICollidable collidable)
	{
		if (collidableObjects.Contains (collidable)) collidableObjects.Remove (collidable);
	}

	public void AddBuilding (Building building)
	{
		if (!buildings.Contains (building)) buildings.Add (building);
	}

	public void RemoveBuilding (Building building)
	{
		if (buildings.Contains (building)) buildings.Remove (building);
	}

	public void AddAsteroid (Asteroid asteroid)
	{
		if (!asteroids.Contains (asteroid)) asteroids.Add (asteroid);
	}

	public void RemoveAsteroid (Asteroid asteroid)
	{
		if (asteroids.Contains (asteroid)) asteroids.Remove (asteroid);
	}

	public List<ISelectable> FindSelectableInRect (Rect rect)
	{
		List<ISelectable> selected = new List<ISelectable> ();

		List<Ship> shipsInRect = agentManager.GetAgentsInRect<Ship> (rect);

		// Try select player ships
		for (int i = 0; i < shipsInRect.Count; i++)
		{
			if (shipsInRect[i].team == playerTeam) selected.Add (shipsInRect[i]);
		}
		if (selected.Count > 0) return selected;

		// Try select player buildings
		List<Building> buildingsInRect = GetBuildingsInRect (rect);

		// Try select player ships
		for (int i = 0; i < buildingsInRect.Count; i++)
		{
			if (buildingsInRect[i].team == playerTeam) selected.Add (buildingsInRect[i]);
		}
		if (selected.Count > 0) return selected;

		// Try get asteroids
		List<Asteroid> asteroidsInRect = GetAsteroidsInRect (rect);
		for (int i = 0; i < asteroidsInRect.Count; i++)
		{
			selected.Add (asteroidsInRect[i]);
		}
		if (selected.Count > 0) return selected;

		// Try select enemy ships
		for (int i = 0; i < shipsInRect.Count; i++)
		{
			if (shipsInRect[i].team == enemyTeam) selected.Add (shipsInRect[i]);
		}
		if (selected.Count > 0) return selected;

		// Try select enemt buildings;
		for (int i = 0; i < buildingsInRect.Count; i++)
		{
			if (buildingsInRect[i].team == enemyTeam) selected.Add (buildingsInRect[i]);
		}

		return selected;
	}

	public List<ISelectable> FindAllSelectableInRect (Rect rect)
	{
		List<ISelectable> selected = new List<ISelectable> ();

		List<Ship> shipsInRect = agentManager.GetAgentsInRect<Ship> (rect);

		// Try select player ships
		for (int i = 0; i < shipsInRect.Count; i++)
		{
			if (shipsInRect[i].team == playerTeam) selected.Add (shipsInRect[i]);
		}

		// Try select player buildings
		List<Building> buildingsInRect = GetBuildingsInRect (rect);

		// Try select player ships
		for (int i = 0; i < buildingsInRect.Count; i++)
		{
			if (buildingsInRect[i].team == playerTeam) selected.Add (buildingsInRect[i]);
		}

		// Try get asteroids
		List<Asteroid> asteroidsInRect = GetAsteroidsInRect (rect);
		for (int i = 0; i < asteroidsInRect.Count; i++)
		{
			selected.Add (asteroidsInRect[i]);
		}

		// Try select enemy ships
		for (int i = 0; i < shipsInRect.Count; i++)
		{
			if (shipsInRect[i].team == enemyTeam) selected.Add (shipsInRect[i]);
		}

		// Try select enemt buildings;
		for (int i = 0; i < buildingsInRect.Count; i++)
		{
			if (buildingsInRect[i].team == enemyTeam) selected.Add (buildingsInRect[i]);
		}

		return selected;
	}

	public bool OnMap (Vector2 position, bool doubleBounds = false)
	{
		if (doubleBounds) return position.x > -size && position.x < size * 2 && position.y > -size && position.y < size * 2;
		else return position.x > 0 && position.x < size && position.y > 0 && position.y < size;
	}

	public List<Asteroid> GetAsteroidsInRect (Rect rect)
	{
		List<Asteroid> inRect = new List<Asteroid> ();

		for (int i = 0; i < asteroids.Count; i++)
		{
			if ((asteroids[i].position - rect.center).sqrMagnitude < asteroids[i].collisionRadius * asteroids[i].collisionRadius
				|| rect.Contains (asteroids[i].position)) inRect.Add (asteroids[i]);
		}

		return inRect;
	}

	public List<Building> GetBuildingsInRect (Rect rect)
	{
		List<Building> inRect = new List<Building> ();

		for (int i = 0; i < buildings.Count; i++)
		{
			if (buildings[i].collisionRectangle.Overlaps (rect)) inRect.Add (buildings[i]);
		}

		return inRect;
	}

	public List<Building> GetBuildingsInRange (Vector2 position, float range, System.Predicate<Building> match)
	{
		List<Building> inRange = new List<Building> ();

		for (int i = 0; i < buildings.Count; i++)
		{
			if (DoesRectIntersectCircle (buildings[i].collisionRectangle, position, range) && match (buildings[i])) inRange.Add (buildings[i]);
		}

		return inRange;
	}

	public static bool DoesRectIntersectCircle (Rect rect, Vector2 circleCenter, float circleRange)
	{
		// This isn't very accurate

		float rangeSqr = circleRange * circleRange;

		if (rect.Contains (circleCenter)) return true;
		if ((circleCenter - new Vector2 (rect.xMin, rect.yMin)).sqrMagnitude < rangeSqr) return true;
		if ((circleCenter - new Vector2 (rect.xMin, rect.yMax)).sqrMagnitude < rangeSqr) return true;
		if ((circleCenter - new Vector2 (rect.xMax, rect.yMax)).sqrMagnitude < rangeSqr) return true;
		if ((circleCenter - new Vector2 (rect.xMax, rect.yMin)).sqrMagnitude < rangeSqr) return true;

		return false;
	}

	public bool CanBuildBuilding (Rect rect)
	{
		for (int i = 0; i < buildings.Count; i++)
		{
			if (!buildings[i].collisionRectangle.Overlaps (rect)) return true;
		}

		return false;
	}

	public Vector2 GetMoveToPosition (Agent agent, float deltaTime)
	{
		Vector2 positionDelta = agent.velocity * deltaTime;

		for (int i = 0; i < collidableObjects.Count; i++)
		{
			if (collidableObjects[i].DoesCollide (agent, deltaTime, ref positionDelta))
			{
				collidableObjects[i].OnCollisionStay (agent);
				agent.OnCollisionStay (collidableObjects[i]);
			}
		}

		Vector2 target = agent.position + Vector2.ClampMagnitude (positionDelta, agent.CurrentMaxSpeed);

		// Clamp the target to the map

		target.x = Mathf.Clamp (target.x, 0, size);
		target.y = Mathf.Clamp (target.y, 0, size);

		return target;
	}
}
