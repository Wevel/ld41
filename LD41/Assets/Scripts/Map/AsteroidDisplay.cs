﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidDisplay : MonoBehaviour
{
	private static Transform holder;
	private static Queue<AsteroidDisplay> unusedDisplayes = new Queue<AsteroidDisplay> ();

	static AsteroidDisplay ()
	{
		holder = new GameObject ("AsteroidDisplayHolder").transform;
	}

	public static void AddDisplay (Asteroid asteroid, Mesh mesh, Material material)
	{
		AsteroidDisplay display;

		if (unusedDisplayes.Count > 0)
		{
			display = unusedDisplayes.Dequeue ();
			display.gameObject.SetActive (true);
		}
		else
		{
			GameObject go = new GameObject ("AsteroidDisplay");
			go.transform.SetParent (holder);
			go.AddComponent<MeshFilter> ();
			go.AddComponent<MeshRenderer> ();
			display = go.AddComponent<AsteroidDisplay> ();
		}

		display.mesh = mesh;
		display.material = material;

		asteroid.SetDisplay (display);
	}

	private static void stopUsingDisplay (AsteroidDisplay display)
	{
		display.gameObject.SetActive (false);
		unusedDisplayes.Enqueue (display);
	}

	public Mesh mesh;
	public Material material;

	private Asteroid asteroid = null;

	private MeshFilter mf;
	private MeshRenderer mr;

	private void Awake ()
	{
		mf = this.GetComponent<MeshFilter> ();
		mr = this.GetComponent<MeshRenderer> ();
	}

	private void Update ()
	{
		if (asteroid != null)
		{
			updatePosition ();
			updateDisplay ();
		}
	}

	public void SetAgent (Asteroid asteroid)
	{
		this.asteroid = asteroid;

		if (asteroid != null)
		{
			updatePosition ();
			updateDisplay ();
		}
		else
		{
			stopUsingDisplay (this);
		}
	}

	private void updatePosition ()
	{
		transform.position = asteroid.position;
		transform.rotation = Quaternion.identity;
	}

	private void updateDisplay ()
	{
		Material[] materials = new Material[mesh.subMeshCount];
		for (int i = 0; i < mesh.subMeshCount; i++) materials[i] = material;
		mr.sharedMaterials = materials;

		if (asteroid == null) mf.mesh = null;
		else mf.mesh = mesh;
	}
}
